# Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# Websockets
https://www.sitepoint.com/implementing-spring-websocket-server-and-client/

## In Spring 4.2, destination variable placeholders are supported it's now possible to do something like:
@MessageMapping("/fleet/{fleetId}/driver/{driverId}")
@SendTo("/topic/fleet/{fleetId}")
public Simple simple(@DestinationVariable String fleetId, @DestinationVariable String driverId) {
    return new Simple(fleetId, driverId);
}

## Other links
https://stackoverflow.com/questions/32826309/how-to-invoke-spring-stomp-web-socket-method-from-rest-client

# Spring :: Current dependencies
[INFO] pl.patro.lukasz:social:jar:0.0.1-SNAPSHOT
[INFO] +- org.springframework.boot:spring-boot-starter-web:jar:1.5.8.RELEASE:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter:jar:1.5.8.RELEASE:compile
[INFO] |  |  +- org.springframework.boot:spring-boot:jar:1.5.8.RELEASE:compile
[INFO] |  |  +- org.springframework.boot:spring-boot-autoconfigure:jar:1.5.8.RELEASE:compile
[INFO] |  |  +- org.springframework.boot:spring-boot-starter-logging:jar:1.5.8.RELEASE:compile
[INFO] |  |  |  +- ch.qos.logback:logback-classic:jar:1.1.11:compile
[INFO] |  |  |  |  \- ch.qos.logback:logback-core:jar:1.1.11:compile
[INFO] |  |  |  +- org.slf4j:jul-to-slf4j:jar:1.7.25:compile
[INFO] |  |  |  \- org.slf4j:log4j-over-slf4j:jar:1.7.25:compile
[INFO] |  |  \- org.yaml:snakeyaml:jar:1.17:runtime
[INFO] |  +- org.springframework.boot:spring-boot-starter-tomcat:jar:1.5.8.RELEASE:compile
[INFO] |  |  +- org.apache.tomcat.embed:tomcat-embed-core:jar:8.5.23:compile
[INFO] |  |  |  \- org.apache.tomcat:tomcat-annotations-api:jar:8.5.23:compile
[INFO] |  |  +- org.apache.tomcat.embed:tomcat-embed-el:jar:8.5.23:compile
[INFO] |  |  \- org.apache.tomcat.embed:tomcat-embed-websocket:jar:8.5.23:compile
[INFO] |  +- org.hibernate:hibernate-validator:jar:5.3.5.Final:compile
[INFO] |  |  +- javax.validation:validation-api:jar:1.1.0.Final:compile
[INFO] |  |  +- org.jboss.logging:jboss-logging:jar:3.3.1.Final:compile
[INFO] |  |  \- com.fasterxml:classmate:jar:1.3.4:compile
[INFO] |  +- com.fasterxml.jackson.core:jackson-databind:jar:2.8.10:compile
[INFO] |  |  +- com.fasterxml.jackson.core:jackson-annotations:jar:2.8.0:compile
[INFO] |  |  \- com.fasterxml.jackson.core:jackson-core:jar:2.8.10:compile
[INFO] |  +- org.springframework:spring-web:jar:4.3.12.RELEASE:compile
[INFO] |  |  +- org.springframework:spring-beans:jar:4.3.12.RELEASE:compile
[INFO] |  |  \- org.springframework:spring-context:jar:4.3.12.RELEASE:compile
[INFO] |  \- org.springframework:spring-webmvc:jar:4.3.12.RELEASE:compile
[INFO] |     \- org.springframework:spring-expression:jar:4.3.12.RELEASE:compile
[INFO] +- org.springframework.boot:spring-boot-starter-data-jpa:jar:1.5.8.RELEASE:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-aop:jar:1.5.8.RELEASE:compile
[INFO] |  |  \- org.aspectj:aspectjweaver:jar:1.8.11:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-jdbc:jar:1.5.8.RELEASE:compile
[INFO] |  |  +- org.apache.tomcat:tomcat-jdbc:jar:8.5.23:compile
[INFO] |  |  |  \- org.apache.tomcat:tomcat-juli:jar:8.5.23:compile
[INFO] |  |  \- org.springframework:spring-jdbc:jar:4.3.12.RELEASE:compile
[INFO] |  +- org.hibernate:hibernate-core:jar:5.0.12.Final:compile
[INFO] |  |  +- org.hibernate.javax.persistence:hibernate-jpa-2.1-api:jar:1.0.0.Final:compile
[INFO] |  |  +- org.javassist:javassist:jar:3.21.0-GA:compile
[INFO] |  |  +- antlr:antlr:jar:2.7.7:compile
[INFO] |  |  +- org.jboss:jandex:jar:2.0.0.Final:compile
[INFO] |  |  +- dom4j:dom4j:jar:1.6.1:compile
[INFO] |  |  \- org.hibernate.common:hibernate-commons-annotations:jar:5.0.1.Final:compile
[INFO] |  +- org.hibernate:hibernate-entitymanager:jar:5.0.12.Final:compile
[INFO] |  +- javax.transaction:javax.transaction-api:jar:1.2:compile
[INFO] |  +- org.springframework.data:spring-data-jpa:jar:1.11.8.RELEASE:compile
[INFO] |  |  +- org.springframework.data:spring-data-commons:jar:1.13.8.RELEASE:compile
[INFO] |  |  +- org.springframework:spring-orm:jar:4.3.12.RELEASE:compile
[INFO] |  |  +- org.springframework:spring-tx:jar:4.3.12.RELEASE:compile
[INFO] |  |  +- org.slf4j:slf4j-api:jar:1.7.25:compile
[INFO] |  |  \- org.slf4j:jcl-over-slf4j:jar:1.7.25:compile
[INFO] |  \- org.springframework:spring-aspects:jar:4.3.12.RELEASE:compile
[INFO] +- mysql:mysql-connector-java:jar:5.1.44:runtime
[INFO] +- org.springframework.boot:spring-boot-starter-security:jar:1.5.8.RELEASE:compile
[INFO] |  +- org.springframework:spring-aop:jar:4.3.12.RELEASE:compile
[INFO] |  +- org.springframework.security:spring-security-config:jar:4.2.3.RELEASE:compile
[INFO] |  |  \- org.springframework.security:spring-security-core:jar:4.2.3.RELEASE:compile
[INFO] |  \- org.springframework.security:spring-security-web:jar:4.2.3.RELEASE:compile
[INFO] \- org.springframework.boot:spring-boot-starter-test:jar:1.5.8.RELEASE:test
[INFO]    +- org.springframework.boot:spring-boot-test:jar:1.5.8.RELEASE:test
[INFO]    +- org.springframework.boot:spring-boot-test-autoconfigure:jar:1.5.8.RELEASE:test
[INFO]    +- com.jayway.jsonpath:json-path:jar:2.2.0:test
[INFO]    |  \- net.minidev:json-smart:jar:2.2.1:test
[INFO]    |     \- net.minidev:accessors-smart:jar:1.1:test
[INFO]    |        \- org.ow2.asm:asm:jar:5.0.3:test
[INFO]    +- junit:junit:jar:4.12:test
[INFO]    +- org.assertj:assertj-core:jar:2.6.0:test
[INFO]    +- org.mockito:mockito-core:jar:1.10.19:test
[INFO]    |  \- org.objenesis:objenesis:jar:2.1:test
[INFO]    +- org.hamcrest:hamcrest-core:jar:1.3:test
[INFO]    +- org.hamcrest:hamcrest-library:jar:1.3:test
[INFO]    +- org.skyscreamer:jsonassert:jar:1.4.0:test
[INFO]    |  \- com.vaadin.external.google:android-json:jar:0.0.20131108.vaadin1:test
[INFO]    +- org.springframework:spring-core:jar:4.3.12.RELEASE:compile
[INFO]    \- org.springframework:spring-test:jar:4.3.12.RELEASE:test

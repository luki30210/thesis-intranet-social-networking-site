package pl.patro.lukasz.social.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.patro.lukasz.social.data.exception.ApplicationUserAlreadyExistsException;
import pl.patro.lukasz.social.data.exception.RoleAlreadyExistsException;
import pl.patro.lukasz.social.data.exception.RoleNotFoundException;
import pl.patro.lukasz.social.data.model.ApplicationUser;
import pl.patro.lukasz.social.data.model.Role;
import pl.patro.lukasz.social.data.service.ApplicationUserService;
import pl.patro.lukasz.social.data.service.RoleService;

import javax.transaction.Transactional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private ApplicationUserService applicationUserService;

    @Autowired
    private RoleService roleService;

    private boolean alreadySetup = false;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (alreadySetup)
            return;

        for (Role.BasicRole role : Role.BasicRole.values()) {
            createRoleIfNotFound(role.toString());
        }

        Role adminRole = null;
        Role userRole = null;
        try {
            adminRole = roleService.findByName(Role.BasicRole.ROLE_ADMIN.toString());
            userRole = roleService.findByName(Role.BasicRole.ROLE_USER.toString());
        } catch (RoleNotFoundException e) {
            e.printStackTrace();
        }

        ApplicationUser admin = new ApplicationUser();
        admin.setUsername("admin");
        admin.setFirstName("Administrator");
        admin.setLastName("Administratorowy");
        admin.setEmail("admin@test.pl");
        admin.setPassword("adminowo");
        admin.setRoles(Stream.of(adminRole, userRole).collect(Collectors.toSet()));

        ApplicationUser user = new ApplicationUser();
        user.setUsername("user");
        user.setFirstName("Uzytkownik");
        user.setLastName("Jakis");
        user.setEmail("user@test.pl");
        user.setPassword("userowo");
        user.setRoles(Stream.of(userRole).collect(Collectors.toSet()));

        try {
            applicationUserService.registerUser(admin);
            applicationUserService.registerUser(user);
        } catch (ApplicationUserAlreadyExistsException e) {
            return;
        }

        alreadySetup = true;
    }

    // @Transactional
    private void createRoleIfNotFound(String roleName) {
        try {
            roleService.findByName(roleName);
        } catch (RoleNotFoundException e) {
            System.out.println("Role " + roleName + " doesn't exist. It will be created.");
            try {
                roleService.addRole(new Role(roleName));
            } catch (RoleAlreadyExistsException e1) {
                e1.printStackTrace();
            }
        }
    }
}
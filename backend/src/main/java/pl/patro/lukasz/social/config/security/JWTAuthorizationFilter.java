package pl.patro.lukasz.social.config.security;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import pl.patro.lukasz.social.data.exception.ApplicationUserNotFoundException;
import pl.patro.lukasz.social.data.model.ApplicationUser;
import pl.patro.lukasz.social.data.service.ApplicationUserService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static pl.patro.lukasz.social.config.security.SecurityConstants.HEADER_STRING;
import static pl.patro.lukasz.social.config.security.SecurityConstants.SECRET;
import static pl.patro.lukasz.social.config.security.SecurityConstants.TOKEN_PREFIX;

class JWTAuthorizationFilter extends BasicAuthenticationFilter {
    private final ApplicationUserService applicationUserService;

    JWTAuthorizationFilter(AuthenticationManager authManager, ApplicationUserService applicationUserService) {
        super(authManager);
        this.applicationUserService = applicationUserService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(HEADER_STRING);

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        try {
            UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (ExpiredJwtException eje) {
            System.out.println("Token expired! There should be redirection to re-login!");
        }

        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);

        if (token != null) {
            String user = Jwts.parser()
                    .setSigningKey(SECRET.getBytes())
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .getBody()
                    .getSubject();

            try {
                ApplicationUser applicationUser = applicationUserService.findByUsername(user);
                List<GrantedAuthority> grantedAuths = new ArrayList<>();
                applicationUser.getRoles().forEach(role -> grantedAuths.add(new SimpleGrantedAuthority(role.getName())));
                return new UsernamePasswordAuthenticationToken(user, null, grantedAuths);
            } catch (ApplicationUserNotFoundException e) {
                e.printStackTrace();
            }

            return null;
        }

        return null;
    }
}

package pl.patro.lukasz.social.config.security;

class SecurityConstants {
    static final String SECRET = "SecretKeyToGenJWTs";

    static final long EXPIRATION_TIME = 864_000_000; // 10 days (10 days * 24 h * 60 min * 60 s * 1000 ms = 864_000_000 ms)

    static final String TOKEN_PREFIX = "Bearer ";

    public static final String HEADER_STRING = "Authorization";

    public static final String REGISTER_URL = "/api/register";

    public static final String LOGIN_URL = "/login";
}

package pl.patro.lukasz.social.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.patro.lukasz.social.data.model.dto.ApplicationUserRolesDTO;
import pl.patro.lukasz.social.data.service.ApplicationUserService;

import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/users")
public class ApplicationUserController {
    @Autowired
    private ApplicationUserService applicationUserService;

    // @GetMapping("/principal")
    // public ResponseEntity<Principal> getPrincipal(Principal principal) {
    //     return new ResponseEntity<>(principal, HttpStatus.OK);
    // }

    @GetMapping
    public ResponseEntity<List<ApplicationUserRolesDTO>> getUsers(@RequestParam(required = false) String sorted, Principal principal) {
        if (sorted != null && sorted.equals("byMessageTimestamp")) {
            return new ResponseEntity<>(applicationUserService.findAllNonDiscardedUsersOrderedByMessageTimestamp(principal.getName()), HttpStatus.OK);
        }
        return new ResponseEntity<>(applicationUserService.findAllNonDiscardedUsers(), HttpStatus.OK);
    }

    @GetMapping("/{username}")
    public ResponseEntity<ApplicationUserRolesDTO> getUser(@PathVariable String username) {
        return new ResponseEntity<>(applicationUserService.findByUsernameAsDto(username), HttpStatus.OK);
    }

    @PatchMapping("/{username}/role/{roleName}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity giveUserRole(@PathVariable String username, @PathVariable String roleName) {
        applicationUserService.giveApplicationUserARole(username, roleName);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{username}/role/{roleName}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity depriveUserRole(@PathVariable String username, @PathVariable String roleName) {
        applicationUserService.depriveApplicationUserOfRole(username, roleName);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<List<ApplicationUserRolesDTO>> getDiscardedUsers() {
        return new ResponseEntity<>(applicationUserService.findAllDiscardedUsers(), HttpStatus.OK);
    }

    @DeleteMapping("/{username}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity discardUser(@PathVariable String username) {
        applicationUserService.discardUser(username);
        return new ResponseEntity(HttpStatus.OK);
    }
}

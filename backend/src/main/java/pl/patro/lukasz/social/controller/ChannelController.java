package pl.patro.lukasz.social.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.patro.lukasz.social.data.model.dto.ChannelDTO;
import pl.patro.lukasz.social.data.model.dto.ChannelEventDTO;
import pl.patro.lukasz.social.data.model.dto.ChannelWithParticipantsDTO;
import pl.patro.lukasz.social.data.service.ChannelEventsService;
import pl.patro.lukasz.social.data.service.ChannelService;

import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/channels")
public class ChannelController {
    @Autowired
    ChannelService channelService;

    @Autowired
    ChannelEventsService channelEventsService;

    @PostMapping
    public ResponseEntity<ChannelDTO> createChannel(Principal principal, @RequestBody String channelName) {
        ChannelDTO channel = channelService.createChannel(channelName, principal.getName());
        return new ResponseEntity<>(channel, HttpStatus.OK);
    }

    @PostMapping("/{channelId}")
    public ResponseEntity<ChannelEventDTO> createMessage(Principal principal, @PathVariable Long channelId, @RequestBody String messageContent) {
        ChannelEventDTO channelEvent = channelEventsService.createMessage(channelId, messageContent, principal.getName());
        return new ResponseEntity<>(channelEvent, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<ChannelDTO>> getChannels(Principal principal) {
        return new ResponseEntity<>(channelService.getChannelsForParticipantOrLeader(principal.getName(), true), HttpStatus.OK);
    }

    @GetMapping("/{channelId}")
    public ResponseEntity<ChannelWithParticipantsDTO> getChannel(Principal principal, @PathVariable Long channelId) {
        return new ResponseEntity<>(channelService.getChannel(principal.getName(), channelId), HttpStatus.OK);
    }

    @GetMapping("/{channelId}/events")
    public ResponseEntity<List<ChannelEventDTO>> getChannelEvents(Principal principal, @PathVariable Long channelId, Pageable page) {
        return new ResponseEntity<>(channelEventsService.getChannelEvents(principal.getName(), channelId, page), HttpStatus.OK);
    }

    @PatchMapping("/{channelId}")
    public ResponseEntity<ChannelWithParticipantsDTO> patchChannel(Principal principal, @RequestBody ChannelWithParticipantsDTO channel, @PathVariable Long channelId) {
        channel.setId(channelId);
        ChannelWithParticipantsDTO updatedChannel = channelService.updateChannel(channel, principal.getName());
        return new ResponseEntity<>(updatedChannel, HttpStatus.OK);
    }
}

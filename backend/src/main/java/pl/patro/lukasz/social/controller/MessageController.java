package pl.patro.lukasz.social.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.patro.lukasz.social.data.exception.MessageNotFoundException;
import pl.patro.lukasz.social.data.model.dto.MessageDTO;
import pl.patro.lukasz.social.data.model.dto.MessageReceivedDTO;
import pl.patro.lukasz.social.data.service.MessageService;

import java.security.Principal;
import java.util.List;


@RestController
@CrossOrigin
@RequestMapping("/api/messages")
public class MessageController {
    @Autowired
    MessageService messageService;

    @PostMapping("/with/{userSendTo}")
    public ResponseEntity<MessageReceivedDTO> sendMessage(@RequestBody String messageContent, Principal principal, @PathVariable String userSendTo) {
        MessageReceivedDTO message = messageService.sendMessage(messageContent, principal.getName(), userSendTo);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @GetMapping("/{messageId}")
    public ResponseEntity<MessageReceivedDTO> getMessage(Principal principal, @PathVariable Long messageId) {
        MessageReceivedDTO message = messageService.getMessage(messageId, principal.getName());
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @GetMapping("/with/{username2}/{page}/{quantity}")
    public ResponseEntity<List<MessageDTO>> getConversation(Principal principal, @PathVariable String username2, @PathVariable int page, @PathVariable int quantity) {
        List<MessageDTO> messages = messageService.getConversation(principal.getName(), username2, page, quantity);
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @GetMapping("/unread")
    public ResponseEntity<List<MessageReceivedDTO>> getAllUnreadMessages(Principal principal) {
        List<MessageReceivedDTO> messages = messageService.getUsersAllUnreadMessages(principal.getName());
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @GetMapping("/unread/{page}/{quantity}")
    public ResponseEntity<List<MessageReceivedDTO>> getUsersAllUnreadMessages(Principal principal, @PathVariable int page, @PathVariable int quantity) {
        List<MessageReceivedDTO> messages = messageService.getUsersAllUnreadMessages(principal.getName(), page, quantity);
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @GetMapping("/unread/with/{userSentBy}")
    public ResponseEntity<List<MessageReceivedDTO>> getConversationsUnreadMessages(Principal principal, @PathVariable String userSentBy) {
        List<MessageReceivedDTO> messages = messageService.getConversationUnreadMessagesForUser(principal.getName(), userSentBy);
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @GetMapping("/unread/with/{userSentBy}/{page}/{quantity}")
    public ResponseEntity<List<MessageReceivedDTO>> getConversationsUnreadMessagesPages(Principal principal, @PathVariable String userSentBy, @PathVariable int page, @PathVariable int quantity) {
        List<MessageReceivedDTO> messages = messageService.getConversationUnreadMessagesForUser(principal.getName(), userSentBy, page, quantity);
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @PutMapping("/{messageId}")
    public ResponseEntity<MessageReceivedDTO> markMessageAsRead(Principal principal, @PathVariable Long messageId) {
        try {
            MessageReceivedDTO message = messageService.markMessageAsReceived(messageId, principal.getName());
            return new ResponseEntity<>(message, HttpStatus.OK);
        } catch (MessageNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping()
    public ResponseEntity<List<MessageReceivedDTO>> markMessagesAsRead(Principal principal, @RequestBody Long[] messageIds) {
        try {
            List<MessageReceivedDTO> messages = messageService.markMessagesAsReceived(messageIds, principal.getName());
            if (messages.size() == messageIds.length) {
                return new ResponseEntity<>(messages, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(messages, HttpStatus.PARTIAL_CONTENT);
            }
        } catch (MessageNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}

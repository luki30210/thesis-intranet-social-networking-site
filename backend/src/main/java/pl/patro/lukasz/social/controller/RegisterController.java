package pl.patro.lukasz.social.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.patro.lukasz.social.data.exception.ApplicationUserNotFoundException;
import pl.patro.lukasz.social.data.model.ApplicationUser;
import pl.patro.lukasz.social.data.service.ApplicationUserService;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/api/register")
public class RegisterController {
    @Autowired
    private ApplicationUserService applicationUserService;


    @PostMapping
    public ResponseEntity<String> signUp(@Valid @RequestBody ApplicationUser applicationUser) {
        applicationUserService.registerUser(applicationUser);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.HEAD)
    public ResponseEntity<Boolean> isUsernameFree(@PathVariable String username) {
       try {
           applicationUserService.findByUsername(username);
       } catch (ApplicationUserNotFoundException e) {
           return new ResponseEntity<>(HttpStatus.OK);
       }
       return new ResponseEntity<>(HttpStatus.CONFLICT);
    }
}

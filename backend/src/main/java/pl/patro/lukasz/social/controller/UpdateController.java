package pl.patro.lukasz.social.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.patro.lukasz.social.data.model.dto.UpdatesDTO;
import pl.patro.lukasz.social.data.service.UpdatesService;

import java.security.Principal;

@RestController
@CrossOrigin
@RequestMapping("/api/updates")
public class UpdateController {
    @Autowired
    private UpdatesService updatesService;

    @GetMapping
    public ResponseEntity<UpdatesDTO> getUpdates(Principal principal) {
        return new ResponseEntity<>(updatesService.getUpdates(principal.getName()), HttpStatus.OK);
    }
}

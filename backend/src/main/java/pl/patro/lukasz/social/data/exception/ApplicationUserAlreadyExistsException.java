package pl.patro.lukasz.social.data.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Łukasz Patro
 * on 11.05.17.
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class ApplicationUserAlreadyExistsException extends RuntimeException {

    public ApplicationUserAlreadyExistsException(String reason) {
        super(reason);
    }

}

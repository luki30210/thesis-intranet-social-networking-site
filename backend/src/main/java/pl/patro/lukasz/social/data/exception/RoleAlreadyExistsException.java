package pl.patro.lukasz.social.data.exception;

/**
 * Created by Łukasz Patro
 * on 11.05.17.
 */
public class RoleAlreadyExistsException extends Exception {

    public RoleAlreadyExistsException(String reason) {
        super(reason);
    }

}

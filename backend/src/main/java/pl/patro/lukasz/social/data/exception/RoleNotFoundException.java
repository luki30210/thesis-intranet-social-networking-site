package pl.patro.lukasz.social.data.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Łukasz Patro
 * on 11.05.17.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RoleNotFoundException extends RuntimeException {

    public RoleNotFoundException(String reason) {
        super(reason);
    }

}

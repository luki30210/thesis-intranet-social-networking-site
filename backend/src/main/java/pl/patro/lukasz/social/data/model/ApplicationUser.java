package pl.patro.lukasz.social.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "users")
public class ApplicationUser {
    @Id
    @Column(name = "username")
    @NotNull
    private String username;

    @Column(name = "password")
    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    private String email;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles", joinColumns = @JoinColumn(name = "user_username", referencedColumnName = "username"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")
    )
    private Set<Role> roles;

    @OneToMany(mappedBy = "leader", fetch = FetchType.LAZY)
    private Set<Channel> channelsLeader;

    @OneToMany(mappedBy = "participant", fetch = FetchType.LAZY)
    private Set<ChannelParticipant> channelsParticipant;

    private Boolean discarded = false;

    @OneToMany(mappedBy = "sentBy")
    private List<Message> sentMessages;

    @OneToMany(mappedBy = "sentTo")
    private List<Message> receivedMessages;
}
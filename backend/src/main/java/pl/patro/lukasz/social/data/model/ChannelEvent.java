package pl.patro.lukasz.social.data.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "channel_events")
public class ChannelEvent {
    public enum EventType { CHANNEL_CREATED, CHANNEL_RENAMED, /*CHANNEL_CLOSED, CHANNEL_OPENED,*/ MESSAGE, USER_ADD, USER_REMOVE }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "channel_id")
    Channel channel;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private ApplicationUser author;

    @NotNull
    private Date timestamp = new Date();

    @NotNull
    @Enumerated(EnumType.STRING)
    private EventType eventType;

    @NotNull
    @Lob
    private String content;
}

package pl.patro.lukasz.social.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "messages")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sent_by")
    private ApplicationUser sentBy;

    @JsonIgnore
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sent_to")
    private ApplicationUser sentTo;

    @NotNull
    @Lob
    private String content;

    @NotNull
    private Date timestamp;

    @NotNull
    private Boolean isReceived;

    public Message() {
        this.timestamp = new Date();
        this.isReceived = false;
    }
}

package pl.patro.lukasz.social.data.model.assembler;

import pl.patro.lukasz.social.data.model.ApplicationUser;
import pl.patro.lukasz.social.data.model.dto.ApplicationUserDTO;
import pl.patro.lukasz.social.data.model.dto.ApplicationUserRolesDTO;

import java.util.List;
import java.util.stream.Collectors;

//@Component
public class ApplicationUserAssembler {
    public static ApplicationUserDTO toApplicationUserDTO(ApplicationUser applicationUser) {
        ApplicationUserDTO applicationUserDTO = new ApplicationUserDTO();
        applicationUserDTO.setUsername(applicationUser.getUsername());
        applicationUserDTO.setEmail(applicationUser.getEmail());
        applicationUserDTO.setFirstName(applicationUser.getFirstName());
        applicationUserDTO.setLastName(applicationUser.getLastName());
        return applicationUserDTO;
    }

    public static List<ApplicationUserDTO> toApplicationUsersDTO(List<ApplicationUser> applicationUsers) {
        return applicationUsers.stream().map(ApplicationUserAssembler::toApplicationUserDTO).collect(Collectors.toList());
    }

    public static ApplicationUserRolesDTO toApplicationUserRolesDTO(ApplicationUser applicationUser) {
        ApplicationUserRolesDTO applicationUserRolesDTO = new ApplicationUserRolesDTO();
        applicationUserRolesDTO.setUsername(applicationUser.getUsername());
        applicationUserRolesDTO.setEmail(applicationUser.getEmail());
        applicationUserRolesDTO.setFirstName(applicationUser.getFirstName());
        applicationUserRolesDTO.setLastName(applicationUser.getLastName());
        applicationUserRolesDTO.setRoles(applicationUser.getRoles());
        return applicationUserRolesDTO;
    }

    public static List<ApplicationUserRolesDTO> toApplicationUsersRolesDTO(List<ApplicationUser> applicationUsers) {
        return applicationUsers.stream().map(ApplicationUserAssembler::toApplicationUserRolesDTO).collect(Collectors.toList());
    }
}

package pl.patro.lukasz.social.data.model.assembler;

import pl.patro.lukasz.social.data.model.ApplicationUser;
import pl.patro.lukasz.social.data.model.Channel;
import pl.patro.lukasz.social.data.model.ChannelParticipant;
import pl.patro.lukasz.social.data.model.dto.ChannelDTO;
import pl.patro.lukasz.social.data.model.dto.ChannelWithParticipantsDTO;

import java.util.List;
import java.util.stream.Collectors;

public class ChannelAssembler {
    public static ChannelDTO assemble(Channel channel) {
        ChannelDTO channelDTO = new ChannelDTO();
        channelDTO.setId(channel.getId());
        channelDTO.setName(channel.getName());
        channelDTO.setLeader(ApplicationUserAssembler.toApplicationUserDTO(channel.getLeader()));
        return channelDTO;
    }

    public static List<ChannelDTO> assemble(List<Channel> channels) {
        return channels.stream().map(ChannelAssembler::assemble).collect(Collectors.toList());
    }

    public static ChannelWithParticipantsDTO assembleChannelLeaderParticipantsDTO(Channel channel) {
        ChannelWithParticipantsDTO channelDTO = new ChannelWithParticipantsDTO();
        channelDTO.setId(channel.getId());
        channelDTO.setName(channel.getName());
        channelDTO.setLeader(ApplicationUserAssembler.toApplicationUserDTO(channel.getLeader()));
        List<ApplicationUser> participants = channel.getChannelParticipants().stream().map(ChannelParticipant::getParticipant).collect(Collectors.toList());
        channelDTO.setChannelParticipants(ApplicationUserAssembler.toApplicationUsersDTO(participants));
        return channelDTO;
    }

    public static List<ChannelWithParticipantsDTO> assembleChannelLeaderParticipantsDTO(List<Channel> channels) {
        return channels.stream().map(ChannelAssembler::assembleChannelLeaderParticipantsDTO).collect(Collectors.toList());
    }
}

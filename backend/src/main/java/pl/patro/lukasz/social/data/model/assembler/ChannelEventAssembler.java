package pl.patro.lukasz.social.data.model.assembler;

import pl.patro.lukasz.social.data.model.ChannelEvent;
import pl.patro.lukasz.social.data.model.dto.ChannelEventDTO;

import java.util.List;
import java.util.stream.Collectors;

public class ChannelEventAssembler {
    public static ChannelEventDTO assemble(ChannelEvent channelEvent) {
        ChannelEventDTO dto = new ChannelEventDTO();
        dto.setAuthor(ApplicationUserAssembler.toApplicationUserDTO(channelEvent.getAuthor()));
        dto.setContent(channelEvent.getContent());
        dto.setEventType(channelEvent.getEventType().name());
        //dto.setId(channelEvent.getId());
        dto.setChannelId(channelEvent.getChannel().getId());
        dto.setTimestamp(channelEvent.getTimestamp());

        return dto;
    }

    public static List<ChannelEventDTO> assemble(List<ChannelEvent> channelEvents) {
        return channelEvents.stream().map(ChannelEventAssembler::assemble).collect(Collectors.toList());
    }
}

package pl.patro.lukasz.social.data.model.assembler;

import pl.patro.lukasz.social.data.model.Message;
import pl.patro.lukasz.social.data.model.dto.MessageDTO;
import pl.patro.lukasz.social.data.model.dto.MessageReceivedDTO;

import java.util.List;
import java.util.stream.Collectors;

//@Component
public class MessageAssembler {
    public static MessageReceivedDTO toMessageReceivedDTO(Message message) {
        MessageReceivedDTO messageReceivedDTO = new MessageReceivedDTO();
        messageReceivedDTO.setId(message.getId());
        messageReceivedDTO.setSentBy(message.getSentBy().getUsername());
        messageReceivedDTO.setTimestamp(message.getTimestamp());
        messageReceivedDTO.setIsReceived(message.getIsReceived());
        messageReceivedDTO.setContent(message.getContent());
        return messageReceivedDTO;
    }

    public static List<MessageReceivedDTO> toMessagesReceivedDTO(List<Message> messages) {
        return messages.stream().map(MessageAssembler::toMessageReceivedDTO).collect(Collectors.toList());
    }

    public static MessageDTO toMessageDTO(Message message) {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setId(message.getId());
        messageDTO.setSentBy(message.getSentBy().getUsername());
        messageDTO.setSentTo(message.getSentTo().getUsername());
        messageDTO.setTimestamp(message.getTimestamp());
        messageDTO.setIsReceived(message.getIsReceived());
        messageDTO.setContent(message.getContent());
        return messageDTO;
    }

    public static List<MessageDTO> toMessagesDTO(List<Message> messages) {
        return messages.stream().map(MessageAssembler::toMessageDTO).collect(Collectors.toList());
    }
}

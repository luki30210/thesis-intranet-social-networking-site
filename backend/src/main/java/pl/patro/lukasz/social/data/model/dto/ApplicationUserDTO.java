package pl.patro.lukasz.social.data.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationUserDTO {
    private String username;

    private String firstName;

    private String lastName;

    private String email;
}
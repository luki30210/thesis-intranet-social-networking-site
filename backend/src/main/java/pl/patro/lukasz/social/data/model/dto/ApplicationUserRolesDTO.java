package pl.patro.lukasz.social.data.model.dto;

import lombok.Getter;
import lombok.Setter;
import pl.patro.lukasz.social.data.model.Role;

import java.util.Set;

@Getter
@Setter
public class ApplicationUserRolesDTO extends ApplicationUserDTO {
    private Set<Role> roles;
}
package pl.patro.lukasz.social.data.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChannelDTO {
    private Long id;
    private String name;
    private ApplicationUserDTO leader;
}

package pl.patro.lukasz.social.data.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ChannelEventDTO {
    // private Long id;
    private Long channelId;
    private ApplicationUserDTO author;
    private Date timestamp;
    private String eventType;
    private String content;
}

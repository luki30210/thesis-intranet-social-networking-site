package pl.patro.lukasz.social.data.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ChannelWithParticipantsDTO extends ChannelDTO {
    private List<ApplicationUserDTO> channelParticipants;
}

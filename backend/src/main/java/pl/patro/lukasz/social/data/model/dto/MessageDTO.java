package pl.patro.lukasz.social.data.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageDTO extends MessageReceivedDTO {
    private String sentTo;
}

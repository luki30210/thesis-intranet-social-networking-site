package pl.patro.lukasz.social.data.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class MessageReceivedDTO {
    private Long id;

    private String sentBy;

    private String content;

    private Date timestamp;

    private Boolean isReceived;
}

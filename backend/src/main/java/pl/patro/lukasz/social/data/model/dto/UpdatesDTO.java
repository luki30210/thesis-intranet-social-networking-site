package pl.patro.lukasz.social.data.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
public class UpdatesDTO {
    private Set<Long> channelIds;
    private Date lastChannelEventTimestamp;
    private Set<String> participantUsernames;
    private Date lastMessageTimestamp;
}

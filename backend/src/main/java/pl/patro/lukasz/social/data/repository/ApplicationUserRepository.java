package pl.patro.lukasz.social.data.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.patro.lukasz.social.data.model.ApplicationUser;

import java.util.List;

public interface ApplicationUserRepository extends CrudRepository<ApplicationUser, String> {
    ApplicationUser findOne(String username);

    List<ApplicationUser> findByDiscardedFalse();

    // @Query("SELECT u FROM ApplicationUser u LEFT JOIN u.sentMessages m ON m.sentTo.username=?1 OR m.sentBy.username=?1 WHERE u.discarded=false GROUP BY u.username ORDER BY MAX(m.timestamp) DESC")
    @Query(nativeQuery = true, value =
            "SELECT *" +
            "FROM thesisdb.users u " +
            "LEFT JOIN thesisdb.messages m ON (u.username = m.sent_by AND m.sent_to = ?1) OR (u.username = m.sent_to AND m.sent_by = ?1) " +
            "WHERE u.discarded = FALSE " +
            "GROUP BY u.username " +
            "ORDER BY MAX(m.timestamp) DESC")
    List<ApplicationUser> findByDiscardedFalseSortedDescByLastMessageTimestamp(String username);

    List<ApplicationUser> findByDiscardedTrue();
}
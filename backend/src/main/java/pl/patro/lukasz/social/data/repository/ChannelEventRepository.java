package pl.patro.lukasz.social.data.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import pl.patro.lukasz.social.data.model.ChannelEvent;

public interface ChannelEventRepository extends CrudRepository<ChannelEvent, Long> {
    Page<ChannelEvent> findAllByChannel_IdOrderByTimestampAsc(Long channelId, Pageable page);
}

package pl.patro.lukasz.social.data.repository;

import org.springframework.data.repository.CrudRepository;
import pl.patro.lukasz.social.data.model.ChannelParticipant;

public interface ChannelParticipantRepository extends CrudRepository<ChannelParticipant, Long> {
}

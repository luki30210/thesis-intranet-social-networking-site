package pl.patro.lukasz.social.data.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.patro.lukasz.social.data.model.Channel;

import java.util.Set;

public interface ChannelRepository extends CrudRepository<Channel, Long> {
    // List<ChannelProjection> findAllByChannelParticipants_participant_usernameOrLeader_username(String participantUsername, String leaderUsername);

    @Deprecated
    Set<Channel> findAllByChannelParticipants_participant_usernameOrLeader_username(String participantUsername, String leaderUsername);

    Set<Channel> findAllByChannelParticipants_participant_username(String participantUsername);
    Set<Channel> findAllByChannelParticipants_participant_usernameOrderByChannelParticipants_lastReadDesc(String participantUsername);

    @Query("SELECT c FROM Channel c LEFT JOIN c.channelParticipants p JOIN c.events e JOIN p.participant u WHERE p.lastRead < e.timestamp AND u.username = ?1")
    Set<Channel> getUnseenEvents(String username);
}

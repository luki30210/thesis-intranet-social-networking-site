package pl.patro.lukasz.social.data.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import pl.patro.lukasz.social.data.model.ApplicationUser;
import pl.patro.lukasz.social.data.model.Message;

import java.util.Collection;
import java.util.List;

public interface MessageRepository extends CrudRepository<Message, Long> {
    @Override
    Message findOne(Long id);

    Message findByIdAndSentBy(Long id, ApplicationUser sentBy);

    List<Message> findBySentToAndIsReceivedFalseOrderByTimestampAsc(ApplicationUser sentTo);

    Page<Message> findBySentToAndIsReceivedFalseOrderByTimestampAsc(ApplicationUser sentTo, Pageable pageable);

    List<Message> findBySentToAndSentByAndIsReceivedFalseOrderByTimestampDesc(ApplicationUser sentTo, ApplicationUser sentBy);

    Page<Message> findBySentToAndSentByAndIsReceivedFalseOrderByTimestampDesc(ApplicationUser sentTo, ApplicationUser sentBy, Pageable pageable);

    Page<Message> findBySentToInAndSentByInOrderByTimestampDesc(Collection<ApplicationUser> sentTo, Collection<ApplicationUser> sentBy, Pageable pageable);
}

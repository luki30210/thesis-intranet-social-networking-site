package pl.patro.lukasz.social.data.repository;

import org.springframework.data.repository.CrudRepository;
import pl.patro.lukasz.social.data.model.Role;

import java.util.List;

public interface RoleRepository extends CrudRepository<Role, Long> {
    Role findByName(String name);

    List<Role> findAll();
}
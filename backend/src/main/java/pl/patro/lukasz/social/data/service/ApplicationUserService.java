package pl.patro.lukasz.social.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.patro.lukasz.social.data.exception.ApplicationUserAlreadyExistsException;
import pl.patro.lukasz.social.data.exception.ApplicationUserNotFoundException;
import pl.patro.lukasz.social.data.exception.RoleNotFoundException;
import pl.patro.lukasz.social.data.model.ApplicationUser;
import pl.patro.lukasz.social.data.model.Role;
import pl.patro.lukasz.social.data.model.assembler.ApplicationUserAssembler;
import pl.patro.lukasz.social.data.model.dto.ApplicationUserRolesDTO;
import pl.patro.lukasz.social.data.repository.ApplicationUserRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ApplicationUserService {
    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public ApplicationUser findByUsername(String username) throws ApplicationUserNotFoundException {
        ApplicationUser applicationUser = applicationUserRepository.findOne(username);

        if (applicationUser != null) {
            return applicationUser;
        } else {
            throw new ApplicationUserNotFoundException("User " + username + " does not exist.");
        }
    }

    public ApplicationUserRolesDTO findByUsernameAsDto(String username) {
        return ApplicationUserAssembler.toApplicationUserRolesDTO(findByUsername(username));
    }

    public List<ApplicationUserRolesDTO> findAllNonDiscardedUsers() {
        List<ApplicationUser> applicationUsers = applicationUserRepository.findByDiscardedFalse();
        return ApplicationUserAssembler.toApplicationUsersRolesDTO(applicationUsers);
    }

    public List<ApplicationUserRolesDTO> findAllNonDiscardedUsersOrderedByMessageTimestamp(String username) {
        List<ApplicationUser> applicationUsers = applicationUserRepository.findByDiscardedFalseSortedDescByLastMessageTimestamp(username);
        return ApplicationUserAssembler.toApplicationUsersRolesDTO(applicationUsers);
    }

    public List<ApplicationUserRolesDTO> findAllDiscardedUsers() {
        List<ApplicationUser> applicationUsers = applicationUserRepository.findByDiscardedTrue();
        return ApplicationUserAssembler.toApplicationUsersRolesDTO(applicationUsers);
    }

    public void registerUser(ApplicationUser newApplicationUser) {
        String username = newApplicationUser.getUsername();

        try {
            this.findByUsername(username);
            throw new ApplicationUserAlreadyExistsException("User " + username + " already exist.");
        } catch (ApplicationUserNotFoundException e) {
            newApplicationUser.setPassword(bCryptPasswordEncoder.encode(newApplicationUser.getPassword()));
            applicationUserRepository.save(newApplicationUser);
        }
    }

    public void discardUser(String username) throws ApplicationUserNotFoundException {
        ApplicationUser au = this.findByUsername(username);
        au.setDiscarded(true);
        final Optional<Role> roleToDelete = au.getRoles().stream().filter(r -> r.getName().equals(Role.BasicRole.ROLE_USER.name())).findFirst();
        roleToDelete.ifPresent(role -> au.getRoles().remove(role));
        applicationUserRepository.save(au);
    }

    public void giveApplicationUserARole(String username, String roleName) throws ApplicationUserNotFoundException, RoleNotFoundException {
        ApplicationUser applicationUser = this.findByUsername(username);
        Role role = roleService.findByName(roleName);
        applicationUser.getRoles().add(role);
        applicationUserRepository.save(applicationUser);
    }

    public void depriveApplicationUserOfRole(String username, String roleName) throws ApplicationUserNotFoundException, RoleNotFoundException {
        ApplicationUser applicationUser = this.findByUsername(username);

        final Optional<Role> roleToDelete = applicationUser.getRoles().stream().filter(r -> r.getName().equals(roleName)).findFirst();
        roleToDelete.ifPresent(role -> applicationUser.getRoles().remove(role));

        applicationUserRepository.save(applicationUser);
    }
}

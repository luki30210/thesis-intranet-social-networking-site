package pl.patro.lukasz.social.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.patro.lukasz.social.data.exception.ChannelNotFoundException;
import pl.patro.lukasz.social.data.exception.NoRightsToChannelException;
import pl.patro.lukasz.social.data.model.Channel;
import pl.patro.lukasz.social.data.model.ChannelEvent;
import pl.patro.lukasz.social.data.model.ChannelParticipant;
import pl.patro.lukasz.social.data.model.assembler.ChannelEventAssembler;
import pl.patro.lukasz.social.data.model.dto.ChannelEventDTO;
import pl.patro.lukasz.social.data.repository.ChannelEventRepository;
import pl.patro.lukasz.social.data.repository.ChannelRepository;

import java.util.Date;
import java.util.List;

@Service
public class ChannelEventsService {
    @Autowired
    ChannelRepository channelRepository;

    @Autowired
    ChannelEventRepository channelEventRepository;

    @Autowired
    ApplicationUserService applicationUserService;

    @Transactional
    public List<ChannelEventDTO> getChannelEvents(String username, Long channelId, Pageable page) {
        final Channel channel = this.findChannel(channelId, username);
        final ChannelParticipant thisParticipant = this.findChannelParticipant(channel, username);
        thisParticipant.setLastRead(new Date());
        channelRepository.save(channel);
        List<ChannelEvent> channelEvents = this.channelEventRepository.findAllByChannel_IdOrderByTimestampAsc(channelId, page).getContent();
        return ChannelEventAssembler.assemble(channelEvents);
    }

    public ChannelEventDTO createMessage(Long channelId, String messageContent, String username) {
        Channel channel = this.findChannel(channelId, username);

        this.findChannelParticipant(channel, username).setLastRead(new Date());


        ChannelEvent channelEvent = new ChannelEvent();
        channelEvent.setChannel(channel);
        channelEvent.setEventType(ChannelEvent.EventType.MESSAGE);
        channelEvent.setContent(messageContent);
        channelEvent.setAuthor(applicationUserService.findByUsername(username));

        channelEvent = channelEventRepository.save(channelEvent);
        return ChannelEventAssembler.assemble(channelEvent);
    }

    private ChannelParticipant findChannelParticipant(Channel channel, String username) {
        final ChannelParticipant thisParticipant = channel.getChannelParticipants().stream().filter(cp -> cp.getParticipant().getUsername().equals(username)).findFirst().orElse(null);
        if (thisParticipant == null) {
            throw new NoRightsToChannelException("User " + username + " is not a channel participant.");
        }
        return thisParticipant;
    }

    private Channel findChannel(Long channelId, String username) {
        Channel channel = channelRepository.findOne(channelId);
        if (channel == null) {
            throw new ChannelNotFoundException("Channel with id = " + channelId + " does not exist.");
        }

        Boolean channelParticipant = channel.getChannelParticipants().stream().anyMatch(cp -> cp.getParticipant().getUsername().equals(username));
        if (!channelParticipant) {
            throw new NoRightsToChannelException("User " + username + " has no rights to read events of channel with id = " + channelId + ".");
        }
        return channel;
    }
}

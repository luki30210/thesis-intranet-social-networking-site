package pl.patro.lukasz.social.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.patro.lukasz.social.data.exception.ChannelNotFoundException;
import pl.patro.lukasz.social.data.exception.NoRightsToChannelException;
import pl.patro.lukasz.social.data.model.ApplicationUser;
import pl.patro.lukasz.social.data.model.Channel;
import pl.patro.lukasz.social.data.model.ChannelEvent;
import pl.patro.lukasz.social.data.model.ChannelParticipant;
import pl.patro.lukasz.social.data.model.assembler.ChannelAssembler;
import pl.patro.lukasz.social.data.model.dto.ApplicationUserDTO;
import pl.patro.lukasz.social.data.model.dto.ChannelDTO;
import pl.patro.lukasz.social.data.model.dto.ChannelWithParticipantsDTO;
import pl.patro.lukasz.social.data.repository.ChannelRepository;
import pl.patro.lukasz.social.data.service.helper.ChannelEventCreator;
import pl.patro.lukasz.social.data.service.helper.ChannelParticipantsHelper;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ChannelService {
    @Autowired
    ChannelRepository channelRepository;

    @Autowired
    ApplicationUserService applicationUserService;

    public ChannelDTO createChannel(String channelName, String channelLeader) {
        Channel newChannel = new Channel();
        ApplicationUser leader = applicationUserService.findByUsername(channelLeader);
        newChannel.setLeader(leader);
        newChannel.setName(channelName);

        Channel savedChannel = channelRepository.save(newChannel);
        List<ChannelEvent> channelEvents = new ArrayList<>();
        channelEvents.add(ChannelEventCreator.prepareChannelCreateEvent(newChannel, leader));

        ChannelParticipant participant = new ChannelParticipant();
        participant.setParticipant(leader);
        participant.setChannel(savedChannel);
        Set<ChannelParticipant> participants = new HashSet<>();
        participants.add(participant);
        savedChannel.setChannelParticipants(participants);

        savedChannel.setEvents(channelEvents);
        return ChannelAssembler.assemble(channelRepository.save(savedChannel));
    }

    @Transactional
    public ChannelWithParticipantsDTO updateChannel(ChannelWithParticipantsDTO updatedChannel, String channelLeader) {
        Channel channel = this.findOne(updatedChannel.getId(), channelLeader);

        List<String> participantsFromUpdatedChannel = updatedChannel.getChannelParticipants().stream().map(ApplicationUserDTO::getUsername).collect(Collectors.toList());
        List<String> participantsToAdd = ChannelParticipantsHelper.determineParticipantUsernamesToAdd(channel, participantsFromUpdatedChannel);
        List<String> participantsToRemove = ChannelParticipantsHelper.determineParticipantUsernamesToRemove(channel, participantsFromUpdatedChannel);

        addNewParticipants(channel, participantsToAdd);

        removeParticipants(channel, participantsToRemove);

        updateChannelName(channel, updatedChannel.getName());

        setChannelLeader(channel, channelLeader);   // updatedChannel.getLeader().getUsername() for new leader -- not implemented

        return ChannelAssembler.assembleChannelLeaderParticipantsDTO(channelRepository.save(channel));
    }

    public List<ChannelDTO> getChannelsForParticipantOrLeader(String username, Boolean sortedByLastRead) {
        applicationUserService.findByUsername(username);
        Set<Channel> channels;
        if (sortedByLastRead) {
            channels = channelRepository.findAllByChannelParticipants_participant_usernameOrderByChannelParticipants_lastReadDesc(username);
        } else {
            channels = channelRepository.findAllByChannelParticipants_participant_username(username);
        }
        return ChannelAssembler.assemble(new ArrayList<>(channels));
    }

    public ChannelWithParticipantsDTO getChannel(String username, Long channelId) {
        Channel channel = this.findOne(channelId);
        Boolean userFound = channel.getLeader().getUsername().equals(username);
        if (!userFound) {
            userFound = channel.getChannelParticipants().stream().anyMatch(cp -> cp.getParticipant().getUsername().equals(username));
        }
        if (userFound) {
            return ChannelAssembler.assembleChannelLeaderParticipantsDTO(channel);
        }
        throw new ChannelNotFoundException("User " + username + " is not a member of channel with id = " + channelId + ".");
    }

    private Channel findOne(Long channelId) {
        Channel channel = channelRepository.findOne(channelId);
        if (channel == null) {
            throw new ChannelNotFoundException("Channel with id = " + channelId + " does not exist.");
        }
        return channel;
    }

    private Channel findOne(Long channelId, String channelLeader) {
        Channel channel = this.findOne(channelId);
        if (!channel.getLeader().getUsername().equals(channelLeader)) {
            throw new NoRightsToChannelException("User " + channelLeader + " is not leader of channel with id = " + channelId + ".");
        }
        return channel;
    }

    private void addNewParticipants(Channel channel, List<String> participantsToAdd) {
        participantsToAdd.forEach(ncp -> {
            channel.getEvents().add(ChannelEventCreator.prepareChannelParticipantAddEvent(channel, ncp, channel.getLeader()));
            ChannelParticipant newCP = new ChannelParticipant();
            newCP.setChannel(channel);
            newCP.setParticipant(applicationUserService.findByUsername(ncp));
            channel.getChannelParticipants().add(newCP);
        });

    }

    private void removeParticipants(Channel channel, List<String> participantsToRemove) {
        participantsToRemove.forEach(participantUsername -> {
            if (participantUsername.equals(channel.getLeader().getUsername())) {
                return;
            }
            channel.getEvents().add(ChannelEventCreator.prepareChannelParticipantRemoveEvent(channel, participantUsername, channel.getLeader()));
            final Optional<ChannelParticipant> channelParticipant = channel.getChannelParticipants().stream().filter(cp -> cp.getParticipant().getUsername().equals(participantUsername)).findFirst();
            channelParticipant.ifPresent(cp -> channel.getChannelParticipants().remove(cp));
        });
    }

    private void updateChannelName(Channel channel, String newChannelName) {
        if (!channel.getName().equals(newChannelName)) {
            channel.setName(newChannelName);
            channel.getEvents().add(ChannelEventCreator.prepareChannelRenameEvent(channel, channel.getLeader()));
        }
    }

    private void setChannelLeader(Channel channel, String leaderUsername) {
        final ApplicationUser user = applicationUserService.findByUsername(leaderUsername);
        channel.setLeader(user);
        final Boolean leaderExistInParticipants = channel.getChannelParticipants().stream().anyMatch(cp -> cp.getParticipant().getUsername().equals(leaderUsername));
        if (!leaderExistInParticipants) {
            final ChannelParticipant channelParticipant = new ChannelParticipant();
            channelParticipant.setChannel(channel);
            channelParticipant.setParticipant(user);
            channel.getChannelParticipants().add(channelParticipant);
        }
    }
}

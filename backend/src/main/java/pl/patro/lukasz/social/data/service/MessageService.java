package pl.patro.lukasz.social.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.patro.lukasz.social.data.exception.ApplicationUserNotFoundException;
import pl.patro.lukasz.social.data.exception.MessageNotFoundException;
import pl.patro.lukasz.social.data.exception.MessageWrongFormatException;
import pl.patro.lukasz.social.data.model.ApplicationUser;
import pl.patro.lukasz.social.data.model.Message;
import pl.patro.lukasz.social.data.model.assembler.MessageAssembler;
import pl.patro.lukasz.social.data.model.dto.MessageDTO;
import pl.patro.lukasz.social.data.model.dto.MessageReceivedDTO;
import pl.patro.lukasz.social.data.repository.MessageRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MessageService {
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private ApplicationUserService applicationUserService;


    private Message getMessage(Long id) throws MessageNotFoundException {
        Message msg = messageRepository.findOne(id);
        if (msg == null) {
            throw new MessageNotFoundException("Message doesn't exist.");
        }
        return msg;
    }

    public MessageDTO getMessage(Long id, String username) throws MessageNotFoundException, ApplicationUserNotFoundException {
        ApplicationUser user = applicationUserService.findByUsername(username);

        Message msg = messageRepository.findByIdAndSentBy(id, user);
        if (msg == null) {
            throw new MessageNotFoundException("Message doesn't exist.");
        }
        return MessageAssembler.toMessageDTO(msg);
    }

    public MessageDTO sendMessage(String content, String sentBy, String sentTo) throws MessageWrongFormatException, ApplicationUserNotFoundException {
        if (sentBy.isEmpty() || sentTo.isEmpty()) {
            throw new MessageWrongFormatException("Sender and receiver cannot be empty.");
        } else if (sentBy.equals(sentTo)) {
            throw new MessageWrongFormatException("Loop message detected.");
        } else if (content.isEmpty()) {
            throw new MessageWrongFormatException("Empty content.");
        }

        ApplicationUser userSentBy = applicationUserService.findByUsername(sentBy);
        ApplicationUser userSentTo = applicationUserService.findByUsername(sentTo);
        Message message = new Message();
        message.setContent(content);
        message.setSentBy(userSentBy);
        message.setSentTo(userSentTo);

        return MessageAssembler.toMessageDTO(messageRepository.save(message));
    }

    public List<MessageDTO> getConversation(String username1, String username2, Integer pageNumber, Integer messagesPerPage) throws ApplicationUserNotFoundException, MessageNotFoundException {
        ApplicationUser user1 = applicationUserService.findByUsername(username1);
        ApplicationUser user2 = applicationUserService.findByUsername(username2);
        List<ApplicationUser> users = Arrays.asList(user1, user2);

        Pageable page = new PageRequest(pageNumber, messagesPerPage);

        List<Message> messages = messageRepository.findBySentToInAndSentByInOrderByTimestampDesc(users, users, page).getContent();

        return MessageAssembler.toMessagesDTO(messages);
    }

    public List<MessageReceivedDTO> getConversationUnreadMessagesForUser(String user, String userSentBy) throws ApplicationUserNotFoundException, MessageNotFoundException {
        ApplicationUser uSentTo = applicationUserService.findByUsername(user);
        ApplicationUser uSentBy = applicationUserService.findByUsername(userSentBy);

        List<Message> messages = messageRepository.findBySentToAndSentByAndIsReceivedFalseOrderByTimestampDesc(uSentTo, uSentBy);

        return MessageAssembler.toMessagesReceivedDTO(messages);
    }

    public List<MessageReceivedDTO> getConversationUnreadMessagesForUser(String user, String userSentBy, Integer pageNumber, Integer messagesPerPage) throws ApplicationUserNotFoundException, MessageNotFoundException {
        ApplicationUser uSentTo = applicationUserService.findByUsername(user);
        ApplicationUser uSentBy = applicationUserService.findByUsername(userSentBy);

        Pageable page = new PageRequest(pageNumber, messagesPerPage);

        List<Message> messages = messageRepository.findBySentToAndSentByAndIsReceivedFalseOrderByTimestampDesc(uSentTo, uSentBy, page).getContent();

        return MessageAssembler.toMessagesReceivedDTO(messages);
    }

    public List<MessageReceivedDTO> getUsersAllUnreadMessages(String user) throws ApplicationUserNotFoundException, MessageNotFoundException {
        ApplicationUser userSentTo = applicationUserService.findByUsername(user);

        List<Message> messages = messageRepository.findBySentToAndIsReceivedFalseOrderByTimestampAsc(userSentTo);

        return MessageAssembler.toMessagesReceivedDTO(messages);
    }

    public List<MessageReceivedDTO> getUsersAllUnreadMessages(String user, Integer pageNumber, Integer messagesPerPage) throws ApplicationUserNotFoundException, MessageNotFoundException {
        ApplicationUser userSentTo = applicationUserService.findByUsername(user);

        Pageable page = new PageRequest(pageNumber, messagesPerPage);

        List<Message> messages = messageRepository.findBySentToAndIsReceivedFalseOrderByTimestampAsc(userSentTo, page).getContent();

        return MessageAssembler.toMessagesReceivedDTO(messages);
    }

    public MessageReceivedDTO markMessageAsReceived(Long messageId, String username) throws MessageNotFoundException, MessageWrongFormatException {
        Message message = this.getMessage(messageId);

        if (!message.getSentTo().getUsername().equals(username)) {
            throw new MessageWrongFormatException("User " + username + " is not the message receiver.");
        }
        message.setIsReceived(true);
        return MessageAssembler.toMessageReceivedDTO(messageRepository.save(message));
    }

    public List<MessageReceivedDTO> markMessagesAsReceived(Long[] messageIds, String username) throws MessageNotFoundException, MessageWrongFormatException {
        if (messageIds.length < 1) {
            throw new MessageNotFoundException("No messages in list.");
        }

        Set<Long> messageIdsSet = Arrays.stream(messageIds).collect(Collectors.toSet());

        List<MessageReceivedDTO> messages = new ArrayList<>();
        messageIdsSet.forEach(msgId -> {
            try {
                MessageReceivedDTO message = markMessageAsReceived(msgId, username);
                messages.add(message);
            } catch (MessageNotFoundException | MessageWrongFormatException e) {
                // suppress
            }
        });

        if (messages.size() > 0) {
            return messages;
        } else {
            throw new MessageNotFoundException("No messages found.");
        }
    }
}

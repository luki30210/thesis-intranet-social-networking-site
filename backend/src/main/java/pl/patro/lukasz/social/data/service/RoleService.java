package pl.patro.lukasz.social.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.patro.lukasz.social.data.exception.RoleAlreadyExistsException;
import pl.patro.lukasz.social.data.exception.RoleNotFoundException;
import pl.patro.lukasz.social.data.model.Role;
import pl.patro.lukasz.social.data.repository.RoleRepository;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    public Role findByName(String name) throws RoleNotFoundException {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            throw new RoleNotFoundException("Role with name '" + name + "' does not exist.");
        }
        return role;
    }

    public void addRole(Role newRole) throws RoleAlreadyExistsException {
        try {
            this.findByName(newRole.getName());
            throw new RoleAlreadyExistsException("Role with name '" + newRole.getName() + "' already exist.");
        } catch (RoleNotFoundException e) {
            roleRepository.save(newRole);
        }
    }
}

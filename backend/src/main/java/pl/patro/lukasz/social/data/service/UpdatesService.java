package pl.patro.lukasz.social.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.patro.lukasz.social.data.model.ApplicationUser;
import pl.patro.lukasz.social.data.model.Channel;
import pl.patro.lukasz.social.data.model.ChannelEvent;
import pl.patro.lukasz.social.data.model.Message;
import pl.patro.lukasz.social.data.model.dto.UpdatesDTO;
import pl.patro.lukasz.social.data.repository.ChannelRepository;
import pl.patro.lukasz.social.data.repository.MessageRepository;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UpdatesService {
    @Autowired
    ChannelRepository channelRepository;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    ApplicationUserService applicationUserService;

    public UpdatesDTO getUpdates(String username) {
        UpdatesDTO updatesDTO = new UpdatesDTO();
        ApplicationUser user = this.findApplicationUser(username);

        final List<Message> unreadMessages = getUnreadMessages(user);
        final Set<Channel> channelsWithUnreadEvents = getChannelsWithUnreadEvents(user);

        final Set<String> unreadMessageAuthors = unreadMessages.stream().map(msg -> msg.getSentBy().getUsername()).collect(Collectors.toSet());
        updatesDTO.setParticipantUsernames(unreadMessageAuthors);
        if (unreadMessageAuthors.size() > 0) {
            updatesDTO.setLastMessageTimestamp(unreadMessages.get(unreadMessages.size() - 1).getTimestamp());
        }

        final Set<Long> channelIdsWithUnreadEvents = channelsWithUnreadEvents.stream().map(Channel::getId).collect(Collectors.toSet());
        updatesDTO.setChannelIds(channelIdsWithUnreadEvents);
        if (channelIdsWithUnreadEvents.size() > 0) {
            final Date lastChannelEventTimestamp = channelsWithUnreadEvents.stream()
                    .map(channel -> channel.getEvents().stream().map(ChannelEvent::getTimestamp).max(Date::compareTo).orElse(null))
                    .max(Date::compareTo).orElse(null);
            updatesDTO.setLastChannelEventTimestamp(lastChannelEventTimestamp);
        }

        return updatesDTO;
    }

    private ApplicationUser findApplicationUser(String username) {
        return this.applicationUserService.findByUsername(username);
    }

    private List<Message> getUnreadMessages(ApplicationUser user) {
        return messageRepository.findBySentToAndIsReceivedFalseOrderByTimestampAsc(user);
    }

    private Set<Channel> getChannelsWithUnreadEvents(ApplicationUser user) {
        return channelRepository.getUnseenEvents(user.getUsername());
    }
}

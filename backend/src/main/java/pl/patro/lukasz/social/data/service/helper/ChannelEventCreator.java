package pl.patro.lukasz.social.data.service.helper;

import pl.patro.lukasz.social.data.model.ApplicationUser;
import pl.patro.lukasz.social.data.model.Channel;
import pl.patro.lukasz.social.data.model.ChannelEvent;

public class ChannelEventCreator {
    public static ChannelEvent prepareChannelCreateEvent(Channel channel, ApplicationUser author) {
        ChannelEvent channelEvent = new ChannelEvent();
        channelEvent.setEventType(ChannelEvent.EventType.CHANNEL_CREATED);
        channelEvent.setAuthor(author);
        channelEvent.setContent(channel.getName());
        channelEvent.setChannel(channel);
        return channelEvent;
    }

    public static ChannelEvent prepareChannelParticipantRemoveEvent(Channel channel, String channelParticipantUsername, ApplicationUser author) {
        ChannelEvent channelEvent = new ChannelEvent();
        channelEvent.setEventType(ChannelEvent.EventType.USER_REMOVE);
        channelEvent.setAuthor(author);
        channelEvent.setContent(channelParticipantUsername);
        channelEvent.setChannel(channel);
        return channelEvent;
    }

    public static ChannelEvent prepareChannelParticipantAddEvent(Channel channel, String channelParticipantUsername, ApplicationUser author) {
        ChannelEvent channelEvent = new ChannelEvent();
        channelEvent.setEventType(ChannelEvent.EventType.USER_ADD);
        channelEvent.setAuthor(author);
        channelEvent.setContent(channelParticipantUsername);
        channelEvent.setChannel(channel);
        return channelEvent;
    }

    public static ChannelEvent prepareChannelRenameEvent(Channel channel, ApplicationUser author) {
        ChannelEvent channelEvent = new ChannelEvent();
        channelEvent.setEventType(ChannelEvent.EventType.CHANNEL_RENAMED);
        channelEvent.setAuthor(author);
        channelEvent.setContent(channel.getName());
        channelEvent.setChannel(channel);
        return channelEvent;
    }
}

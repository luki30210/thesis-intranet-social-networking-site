package pl.patro.lukasz.social.data.service.helper;

import pl.patro.lukasz.social.data.model.Channel;

import java.util.ArrayList;
import java.util.List;

public class ChannelParticipantsHelper {
    public static List<String> determineParticipantUsernamesToAdd(final Channel channel, final List<String> newUsersList) {
        List<String> usersToAdd = new ArrayList<>();
        newUsersList.forEach(nu -> {
            Boolean alreadyExist = channel.getChannelParticipants().stream().anyMatch(cp -> cp.getParticipant().getUsername().equals(nu));
            if (!alreadyExist) {
                usersToAdd.add(nu);
            }
        });
        return usersToAdd;
    }

    public static List<String> determineParticipantUsernamesToRemove(final Channel channel, final List<String> newUsersList) {
        List<String> usersToRemove = new ArrayList<>();
        channel.getChannelParticipants().forEach(cp -> {
            String cpUsername = cp.getParticipant().getUsername();
            Boolean existInNewList = newUsersList.stream().anyMatch(nu -> nu.equals(cpUsername));
            if (!existInNewList) {
                usersToRemove.add(cpUsername);
            }
        });
        return usersToRemove;
    }
}

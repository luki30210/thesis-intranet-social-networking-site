import { Channel } from './channel';

export interface AppPage {
  name?: string;
  description?: string;
  channelReference?: Channel;
}

export { Channel };

import { EventType } from './event-type.enum';
import { User } from './user';

export interface ChannelEvent {
  // id?: number;
  channelId?: number;
  author?: User;
  timestamp?: Date;
  eventType: EventType;
  content?: string;
}

export { EventType, User };

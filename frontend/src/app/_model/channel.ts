import { User } from './user';
import { ChannelEvent } from './channel-event';

export interface Channel {
  id?: number;
  name?: string;
  leader?: User;
  channelParticipants?: User[];
  events?: ChannelEvent[];
  newEvents?: boolean;
}

export { ChannelEvent, User };

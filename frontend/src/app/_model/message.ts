import { User } from './user';

export interface Message {
  id?: number;
  sentBy?: string;
  sentTo?: string;
  content?: string;
  timestamp?: Date;
  isReceived?: boolean;
}

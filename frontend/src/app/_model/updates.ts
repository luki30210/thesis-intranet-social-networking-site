export class Updates {
  participantUsernames?: string[];
  lastMessageTimestamp?: Date;
  channelIds?: number[];
  lastChannelEventTimestamp?: Date;
}

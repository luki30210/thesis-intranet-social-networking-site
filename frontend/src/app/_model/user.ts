import { Role } from './role';

export class User {
  username?: string;
  password?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  roles?: Role[];
  newMessages?: boolean;
}

export { Role };

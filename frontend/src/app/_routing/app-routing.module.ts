import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoggedInCanActivateService } from '../_service/security/logged-in-can-activate.service';
import { NotLoggedInCanActivateService } from '../_service/security/not-logged-in-can-activate.service';
import { AdminCanActivateService } from '../_service/security/admin-can-activate.service';

import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { ManageUsersComponent } from '../manage-users/manage-users.component';
import { ConversationComponent } from '../conversation/conversation.component';
import { ChannelComponent } from '../channel/channel.component';

const routes: Routes = [
  // { path: '', redirectTo: '/', pathMatch: 'full' },
  {
    path: '', component: HomeComponent, canActivate: [LoggedInCanActivateService], children: [
      // { path: 'logout', component: LogoutComponent, pathMatch: 'full'}
      // { path: 'conversation/:userId', component: ConversationComponent, pathMatch: 'full'}
      { path: 'manage-users', component: ManageUsersComponent, canActivate: [AdminCanActivateService], pathMatch: 'full' },
      { path: 'conversation/:username', component: ConversationComponent, pathMatch: 'full'},
      { path: 'channel/:id', component: ChannelComponent, pathMatch: 'full'}
    ]
  },
  { path: 'log-in', component: LoginComponent, pathMatch: 'full', canActivate: [NotLoggedInCanActivateService] },
  { path: 'register', component: RegisterComponent, pathMatch: 'full', canActivate: [NotLoggedInCanActivateService] },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: [LoggedInCanActivateService, NotLoggedInCanActivateService, AdminCanActivateService]
})
export class AppRoutingModule { }

export const routingComponents = [
  HomeComponent,
  LoginComponent,
  RegisterComponent,
  ManageUsersComponent,
  ConversationComponent,
  ChannelComponent
];

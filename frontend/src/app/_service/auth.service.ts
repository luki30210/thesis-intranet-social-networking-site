import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
// import { HttpHeaders, HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';

import { AppSettings } from '../app.settings';
import { User } from '../_model/user';

@Injectable()
export class AuthService {
  private headers: Headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private http: Http) { }

  login(user: User): Promise<Response> {
    const url = './login';
    return this.http.post(url, user, { headers: this.headers }).toPromise();
  }

  register(user: User): Promise<Response> {
    const url = `${AppSettings.API_ENDPOINT}/register`;
    return this.http.post(url, user, { headers: this.headers }).toPromise();
  }

  logout(): void {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
  }

  isUsernameFree(username: string): Promise<Response> {
    const url = `${AppSettings.API_ENDPOINT}/register/${username}`;
    return this.http.head(url, { headers: this.headers })
      // .map(resp => resp.status === 200)
      .toPromise();
  }
}

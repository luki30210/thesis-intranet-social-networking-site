import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

import { AppSettings } from '../app.settings';
import { Channel, ChannelEvent } from '../_model/channel';

@Injectable()
export class ChannelService {

  private token: string = localStorage.getItem('token');
  private headers: Headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.token });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) { }

  postChannel(channelName: string): Promise<Channel> {
    const url = `${AppSettings.API_ENDPOINT}/channels`;
    return this.http.post(url, channelName, this.options)
      .map((response: Response) => response.json())
      .toPromise();
  }

  sendMessage(channelId: number, messageContent: string): Promise<ChannelEvent> {
    const url = `${AppSettings.API_ENDPOINT}/channels/${channelId}`;
    return this.http.post(url, messageContent, this.options)
      .map((response: Response) => {
        const channelEvent: ChannelEvent = response.json();
        channelEvent.timestamp = new Date(channelEvent.timestamp);
        return channelEvent;
      })
      .toPromise();
  }

  getChannels(): Promise<Channel[]> {
    const url = `${AppSettings.API_ENDPOINT}/channels`;
    return this.http.get(url, this.options)
      .map((response: Response) => response.json())
      .toPromise();
  }

  getChannel(channelId: number): Promise<Channel> {
    const url = `${AppSettings.API_ENDPOINT}/channels/${channelId}`;
    return this.http.get(url, this.options)
      .map((response: Response) => response.json())
      .toPromise();
  }

  getChannelEvents(channelId: number, pageNumber: number, pageSize: number): Promise<ChannelEvent[]> {
    const url = `${AppSettings.API_ENDPOINT}/channels/${channelId}/events?page=${pageNumber}&size=${pageSize}`;
    return this.http.get(url, this.options)
      .map((response: Response) => {
        const channelEvents: ChannelEvent[] = response.json();
        channelEvents.forEach(ce => ce.timestamp = new Date(ce.timestamp));
        return channelEvents;
      })
      .toPromise();
  }

  patchChannel(channel: Channel): Promise<Channel> {
    const url = `${AppSettings.API_ENDPOINT}/channels/${channel.id}`;
    return this.http.patch(url, channel, this.options)
      .map((response: Response) => response.json())
      .toPromise();
  }

}

export { Channel };

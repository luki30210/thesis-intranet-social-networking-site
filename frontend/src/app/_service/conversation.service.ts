import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

import { AppSettings } from '../app.settings';

import { User } from '../_model/user';
import { Message } from '../_model/message';

@Injectable()
export class ConversationService {

  private token: string = localStorage.getItem('token');
  private headers: Headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.token });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) { }

  sendMessage(usernameTo: string, messageContent: string): Promise<Message> {
    const url = `${AppSettings.API_ENDPOINT}/messages/with/${usernameTo}`;
    return this.http.post(url, messageContent, this.options)
      .map((response: Response) => {
        const message: Message = response.json();
        message.timestamp = new Date(message.timestamp);
        return message;
      })
      .toPromise();
  }

  getConversation(usernameTo: string, page: number, messagesPerPage: number): Promise<Message[]> {
    const url = `${AppSettings.API_ENDPOINT}/messages/with/${usernameTo}/${page}/${messagesPerPage}`;
    return this.http.get(url, this.options)
      .map((response: Response) => {
        const messages: Message[] = response.json();
        messages.forEach(msg => msg.timestamp = new Date(msg.timestamp));
        return messages;
      })
      .toPromise();
  }

  getUnreadMessages(): Promise<Message[]> {
    const url = `${AppSettings.API_ENDPOINT}/messages/unread`;
    return this.http.get(url, this.options)
      .map((response: Response) => {
        const messages: Message[] = response.json();
        messages.forEach(msg => msg.timestamp = new Date(msg.timestamp));
        return messages;
      })
      .toPromise();
  }

  getConversationUnreadMessages(usernameTo: string): Promise<Message[]> {
    const url = `${AppSettings.API_ENDPOINT}/messages/unread/with/${usernameTo}`;
    return this.http.get(url, this.options)
      .map((response: Response) => {
        const messages: Message[] = response.json();
        messages.forEach(msg => msg.timestamp = new Date(msg.timestamp));
        return messages;
      })
      .toPromise();
  }

  markMessageAsReceived(messageId: number): Promise<Message> {
    const url = `${AppSettings.API_ENDPOINT}/messages/${messageId}`;
    return this.http.put(url, this.options)
      .map((response: Response) => {
        const message: Message = response.json();
        message.timestamp = new Date(message.timestamp);
        return message;
      })
      .toPromise();
  }

  markMessagesAsReceived(messageIds: number[]): Promise<Message[]> {
    const url = `${AppSettings.API_ENDPOINT}/messages`;
    return this.http.put(url, messageIds, this.options)
      .map((response: Response) => {
        const messages: Message[] = response.json();
        messages.forEach(msg => msg.timestamp = new Date(msg.timestamp));
        return messages;
      })
      .toPromise();
  }

}

export { Message };

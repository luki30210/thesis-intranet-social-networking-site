import { Injectable } from '@angular/core';

@Injectable()
export class NotificationService {

  private notificationSound = new Audio('../../assets/notification.mp3');
  private iconAddress = '../../assets/paper-plane-3.png';

  constructor() { }

  sendNotification(message: string): void {
    if ((<any>window).Notification /*&& (<any>Notification).permission !== 'denied'*/) {
      Notification.requestPermission(function(status) {  // status is "granted", if accepted by user
        const n = new Notification('Intranetowy serwis społecznościowy', {
          body: message
          // icon: '../../assets/paper-plane-3.png' // optional
        });
      });
    }
    this.notificationSound.play();
  }

}

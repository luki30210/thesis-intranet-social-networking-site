import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AppSettings } from '../../app.settings';
import { User } from '../../_model/user';

@Injectable()
export class AdminCanActivateService implements CanActivate {
  constructor(private router: Router) { }

  canActivate(): boolean {
    const currentUser: User = JSON.parse(localStorage.getItem('currentUser'));
    if (!currentUser || currentUser.roles.find(r => r.name === AppSettings.ROLE_ADMIN) === undefined) {
      this.router.navigateByUrl('/');
      return false;
    }
    return true;
  }
}

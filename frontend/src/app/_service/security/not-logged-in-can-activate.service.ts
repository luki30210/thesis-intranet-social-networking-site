import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class NotLoggedInCanActivateService {

  constructor(private router: Router) { }

  canActivate(): boolean {
    if (localStorage.getItem('currentUser')) {
      this.router.navigateByUrl('/');
      return false;
    } else {
      return true;
    }
  }

}

import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';

import { AppSettings } from '../app.settings';
import { Updates } from '../_model/updates';

@Injectable()
export class UpdatesService {

  private token: string = localStorage.getItem('token');
  private headers: Headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.token });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) { }

  getUpdates(): Promise<Updates> {
    const url = `${AppSettings.API_ENDPOINT}/updates`;
    return this.http.get(url, this.options)
      .map((response: Response) => response.json())
      .toPromise();
  }

}

export { Updates };

import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

import { AppSettings } from '../app.settings';

import { User, Role } from '../_model/user';

@Injectable()
export class UserService {

  private token: string = localStorage.getItem('token');
  private headers: Headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.token });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) { }

  static haveRoleUser(user: User): boolean {
    return user.roles.find(r => r.name === AppSettings.ROLE_USER) !== undefined;
  }

  getAllUsers(): Promise<User[]> {
    const url = `${AppSettings.API_ENDPOINT}/users`;
    return this.http.get(url, this.options)
      .map((response: Response) => response.json())
      .toPromise();
  }

  getAllUsersSortedByConversationTimestamp(): Promise<User[]> {
    const url = `${AppSettings.API_ENDPOINT}/users?sorted=byMessageTimestamp`;
    return this.http.get(url, this.options)
      .map((response: Response) => response.json())
      .toPromise();
  }

  getUser(username: string): Promise<User> {
    const url = `${AppSettings.API_ENDPOINT}/users/${username}`;
    return this.http.get(url, this.options)
      .map((response: Response) => response.json())
      .toPromise();
  }

  giveUserRole(username: String, roleName: String): Promise<Response> {
    const url: string = AppSettings.API_ENDPOINT + '/users/' + username + '/role/' + roleName;
    return this.http.patch(url, null, this.options)
      .toPromise();
  }

  depriveUserRole(username: String, roleName: String): Promise<Response> {
    const url: string = AppSettings.API_ENDPOINT + '/users/' + username + '/role/' + roleName;
    return this.http.delete(url, this.options)
      .toPromise();
  }

  getDiscardedUser(): Promise<User[]> {
    const url: string = AppSettings.API_ENDPOINT + '/users';
    return this.http.delete(url, this.options)
      .map((response: Response) => response.json())
      .toPromise();
  }

  discardUser(username: String): Promise<Response> {
    const url: string = AppSettings.API_ENDPOINT + '/users/' + username;
    return this.http.delete(url, this.options)
      .toPromise();
  }

}

export { User, Role };

import { Component, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Channel } from '../../../_service/channel.service';

@Component({
  templateUrl: './channel-designer-dialog.component.html'
  // styleUrls: ['./channel-designer-dialog.component.css']
})
export class ChannelDesignerDialogComponent {

  channelNameFormControl: FormControl;
  editMode = false;
  editedChannel: Channel;

  constructor(
    public dialogRef: MatDialogRef<ChannelDesignerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data && data.channel) {
      this.editMode = true;
      this.editedChannel = (<Channel>data.channel);
      this.channelNameFormControl = new FormControl(this.editedChannel.name, [Validators.required]);
    } else {
      this.channelNameFormControl = new FormControl('', [Validators.required]);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmitClick(): void {
    if (this.editMode) {
      const editedChannel: Channel = {
        name: this.channelNameFormControl.value,
        id: this.editedChannel.id,
        channelParticipants: this.editedChannel.channelParticipants,
        leader: this.editedChannel.leader
      };
      this.dialogRef.close(editedChannel);
    } else {
      const newChannel: Channel = {
        name: this.channelNameFormControl.value
      };
      this.dialogRef.close(newChannel);
    }
  }

}

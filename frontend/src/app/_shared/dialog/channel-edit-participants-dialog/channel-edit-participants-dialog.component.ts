import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';

import { User } from '../../../_service/user.service';
import { Channel } from '../../../_service/channel.service';

@Component({
  selector: 'app-channel-edit-participants-dialog',
  templateUrl: './channel-edit-participants-dialog.component.html',
  styleUrls: ['./channel-edit-participants-dialog.component.css']
})
export class ChannelEditParticipantsDialogComponent implements OnInit {

  participants: User[];
  allUsers: User[];

  searchUserFormControl = new FormControl();
  filteredUsers: Observable<any[]>;

  displayedColumns = ['username', 'firstName', 'lastName', 'actions'];
  dataSource: MatTableDataSource<User[]>;

  constructor(
    public dialogRef: MatDialogRef<ChannelEditParticipantsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: { channel: Channel, allUsers: User[] }
  ) {
    this.participants = JSON.parse(JSON.stringify(data.channel.channelParticipants))
      .filter(participant => participant.username !== data.channel.leader.username);

    this.allUsers = JSON.parse(JSON.stringify(data.allUsers));
    this.allUsers = this.allUsers.filter(user => {
      const isLeader = user.username === data.channel.leader.username;
      if (isLeader) {
        return false;
      }
      return this.participants.findIndex(p => p.username === user.username) === -1;
    });

    this.refreshData();
  }

  ngOnInit() { }

  participantAddClick(username: string) {
    const index = this.allUsers.findIndex(p => p.username === username);
    if (index === -1) {
      return;
    }
    const addedUser = this.allUsers.splice(index, 1)[0];
    if (addedUser) {
      this.participants.push(addedUser);
    }
    this.refreshData();
    this.searchUserFormControl.setValue('');
  }

  participantRemoveClick(username: string) {
    const index = this.participants.findIndex(p => p.username === username);
    if (index === -1) {
      return;
    }
    const removedUser = this.participants.splice(index, 1)[0];
    if (removedUser) {
      this.allUsers.push(removedUser);
    }
    this.refreshData();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSaveClick(): void {
    this.dialogRef.close(this.participants);
  }

  private filterUsers(username: string) {
    return this.allUsers.filter(u => {
      const searchData = u.firstName + u.lastName + u.email + u.username;
      return searchData.toLowerCase().indexOf(username.toLowerCase()) !== -1;
    });
  }

  private prepareFilteredUsers() {
    this.filteredUsers = this.searchUserFormControl.valueChanges.pipe(
      startWith(''),
      map(username => username ? this.filterUsers(username) : this.allUsers.slice())
    );
  }

  private refreshData() {
    this.dataSource = new MatTableDataSource<any>(this.participants);
    this.prepareFilteredUsers();
  }

}

import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';

import { User, UserService } from '../../../_service/user.service';

@Component({
  selector: 'app-users-trash-dialog',
  templateUrl: './users-trash-dialog.component.html',
  styleUrls: ['./users-trash-dialog.component.css']
})
export class UsersTrashDialogComponent {

  discardedUsers: User[];

  displayedColumns = ['username', 'firstName', 'lastName'/*, 'actions'*/];
  dataSource: MatTableDataSource<User[]>;

  constructor(
    public dialogRef: MatDialogRef<UsersTrashDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: User[],
    private userService: UserService
  ) {
    this.refreshData();
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  private refreshData() {
    this.userService.getDiscardedUser()
      .then(discardedUsers => {
        this.dataSource = new MatTableDataSource<any>(discardedUsers);
      })
      .catch(err => console.log(err));
  }

}

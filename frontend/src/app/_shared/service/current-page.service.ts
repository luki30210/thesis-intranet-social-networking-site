import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { AppPage } from '../../_model/app-page';
import { User } from '../../_model/user';
import { Channel } from '../../_model/channel';

@Injectable()
export class CurrentPageService {

  private emitChangeSource = new Subject<AppPage>();
  changeEmitted$ = this.emitChangeSource.asObservable();

  static prepareConversationAppPageInfo(participant: User): AppPage {
    const currentPage: AppPage = {
      name: 'Konwersacja',
      description: participant.firstName + ' ' + participant.lastName
    };
    return currentPage;
  }

  static prepareChannelAppPageInfo(channel: Channel): AppPage {
    let participantsList: string = channel.channelParticipants
      .filter(cp => cp.username !== channel.leader.username)
      .map(cp => cp.firstName + ' ' + cp.lastName).join(', ');
    const leaderFullName: string = '<b>' + channel.leader.firstName + ' ' + channel.leader.lastName + '</b>';
    if (participantsList.length > 0) {
      participantsList = leaderFullName + ', ' + participantsList;
    } else {
      participantsList = leaderFullName;
    }

    const currentPage: AppPage = {
      channelReference: channel,
      description: participantsList,
      name: channel.name
    };
    return currentPage;
  }

  emitChange(change: AppPage) {
    this.emitChangeSource.next(change);
  }

}

export { AppPage };

import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './_material/material.module';
// import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AuthService } from './_service/auth.service';
import { ChannelService } from './_service/channel.service';
import { CurrentPageService } from './_shared/service/current-page.service';
import { ConversationService } from './_service/conversation.service';
import { NotificationService } from './_service/notification.service';
import { UpdatesService } from './_service/updates.service';
import { UsernameValidator } from './register/validator/username.validator';
import { UserService } from './_service/user.service';

import { AppComponent } from './app.component';
import { AppRoutingModule, routingComponents } from './_routing/app-routing.module';
import { ConfirmDialogComponent } from './_shared/dialog/confirm-dialog/confirm-dialog.component';
import { SidebarContactsComponent } from './sidebar-contacts/sidebar-contacts.component';
import { SidebarChannelsComponent } from './sidebar-channels/sidebar-channels.component';
import { ChannelDesignerDialogComponent } from './_shared/dialog/channel-designer-dialog/channel-designer-dialog.component';
import {
  ChannelEditParticipantsDialogComponent
} from './_shared/dialog/channel-edit-participants-dialog/channel-edit-participants-dialog.component';
import { UsersTrashDialogComponent } from './_shared/dialog/users-trash-dialog/users-trash-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    ConfirmDialogComponent,
    ChannelEditParticipantsDialogComponent,
    ChannelDesignerDialogComponent,
    SidebarContactsComponent,
    SidebarChannelsComponent,
    UsersTrashDialogComponent
  ],
  entryComponents: [
    ConfirmDialogComponent,
    ChannelEditParticipantsDialogComponent,
    ChannelDesignerDialogComponent,
    UsersTrashDialogComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MaterialModule
    // AngularFontAwesomeModule
  ],
  providers: [
    AuthService,
    ChannelService,
    CurrentPageService,
    ConversationService,
    NotificationService,
    UpdatesService,
    UsernameValidator,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

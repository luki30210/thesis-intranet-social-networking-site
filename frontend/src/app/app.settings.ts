export class AppSettings {
  public static API_ENDPOINT = './api';

  public static ROLE_ADMIN = 'ROLE_ADMIN';
  public static ROLE_USER = 'ROLE_USER';
  public static PASSWORD_MIN_LENGTH = 6;

  public static CONVERSATION_MESSAGES_PER_PAGE = 40;
  public static CHANNEL_MESSAGES_PER_PAGE = 60;
  public static REFRESH_INTERVAL = 1000;
}

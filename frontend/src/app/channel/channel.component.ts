import { AfterViewChecked, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';

import { AppSettings } from '../app.settings';
import { Channel, ChannelService } from '../_service/channel.service';
import { CurrentPageService } from '../_shared/service/current-page.service';
import { User } from '../_service/user.service';
import { Updates } from '../_service/updates.service';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelComponent implements AfterViewChecked, OnDestroy, OnInit {

  channel: Channel;
  currentUser: User;

  scrollToBottomBool: boolean;
  refreshObservableSubscription: Subscription;

  constructor(
    private aRoute: ActivatedRoute,
    private router: Router,
    private channelService: ChannelService,
    private currentPageService: CurrentPageService
  ) { }

  ngOnInit() {
    this.aRoute.params.subscribe((params: Params) => this.getCurrentChannel(params['id']));
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.refreshObservableSubscription === undefined || this.refreshObservableSubscription.closed) {
      this.startRefreshObservable();
    }
  }

  ngAfterViewChecked() {
    if (this.scrollToBottomBool) {
      this.scrollToBottom();
      this.scrollToBottomBool = false;
    }
  }

  ngOnDestroy() {
    this.refreshObservableSubscription.unsubscribe();
  }

  sendMessage(textarea: any): void {
    if (textarea.value !== '') {
      this.channelService.sendMessage(this.channel.id, textarea.value)
        .then(event => {
          this.channel.events.push(event);
          this.sortChannelEvents();
          textarea.value = '';
          this.scrollToBottomBool = true;
        })
        .catch(err => console.log(err));
    }
  }

  onKeyEnterMessage(textarea: any, event: any) {
    if (event.shiftKey) {
      textarea.value += '\n';
    } else {
      this.sendMessage(textarea);
    }
  }

  private getCurrentChannel(channelId: number): void {
    this.channelService.getChannel(channelId)
      .then(channel => {
        this.channel = channel;
        this.currentPageService.emitChange(CurrentPageService.prepareChannelAppPageInfo(this.channel));
        this.getChannelEvents(channelId);
      })
      .catch(err => console.log(err)
      );
  }

  private getChannelEvents(channelId: number) {
    this.channelService.getChannelEvents(channelId, 0, AppSettings.CHANNEL_MESSAGES_PER_PAGE)
      .then(events => {
        this.channel.events = events;
      })
      .catch(err => console.log(err));
  }

  private sortChannelEvents() {
    this.channel.events = this.channel.events.sort((c1, c2) => c1.timestamp.valueOf() - c2.timestamp.valueOf());
  }

  private scrollToBottom() {
    const chatElement: HTMLElement = document.getElementById('chat');
    chatElement.scrollTop = chatElement.scrollHeight;
  }

  private startRefreshObservable() {
    this.refreshObservableSubscription = Observable.interval(AppSettings.REFRESH_INTERVAL / 2).takeWhile(() => true)
      .subscribe(() => {
        const updates: Updates = JSON.parse(localStorage.getItem('updates'));
        if (!updates) {
          return;
        }

        if (updates.channelIds.findIndex(chid => chid === this.channel.id) !== -1) {
          this.getChannelEvents(this.channel.id);
        }
      });
  }

}

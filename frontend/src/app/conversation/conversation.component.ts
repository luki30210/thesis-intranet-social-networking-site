import { Component, OnDestroy, OnInit, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/switchMap';

import { AppSettings } from '../app.settings';

import { UserService, User } from '../_service/user.service';
import { Updates } from '../_service/updates.service';
import { ConversationService, Message } from '../_service/conversation.service';
import { CurrentPageService, AppPage } from '../_shared/service/current-page.service';

@Component({
  templateUrl: './conversation.component.html',
  styleUrls: ['../channel/channel.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConversationComponent implements OnDestroy, OnInit, AfterViewChecked {

  currentUser: User;
  participant: User = new User();
  conversation: Message[] = [];
  scrollToBottomBool: boolean;
  refreshObservableSubscription: Subscription;

  constructor(
    private aRoute: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private conversationService: ConversationService,
    private currentPageService: CurrentPageService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.aRoute.params.subscribe((params: Params) => this.getParticipant(params['username']));
    if (this.refreshObservableSubscription === undefined || this.refreshObservableSubscription.closed) {
      this.startRefreshObservable();
    }
  }

  ngOnDestroy() {
    this.refreshObservableSubscription.unsubscribe();
  }

  ngAfterViewChecked() {
    if (this.scrollToBottomBool) {
      this.scrollToBottom();
      this.scrollToBottomBool = false;
    }
  }

  sendMessage(textarea: any): void {
    if (textarea.value !== '') {
      this.conversationService.sendMessage(this.participant.username, textarea.value)
        .then(msg => {
          this.conversation.push(msg);
          this.sortConversation();
          textarea.value = '';
          this.scrollToBottomBool = true;
        })
        .catch(err => console.log(err));
    }
  }

  onKeyEnterMessage(textarea: any, event: any) {
    if (event.shiftKey) {
      textarea.value += '\n';
    } else {
      this.sendMessage(textarea);
    }
  }

  private sortConversation() {
    this.conversation = this.conversation.sort((c1, c2) => c1.timestamp.valueOf() - c2.timestamp.valueOf());
  }

  private scrollToBottom() {
    const chatElement: HTMLElement = document.getElementById('chat');
    chatElement.scrollTop = chatElement.scrollHeight;
  }

  private getParticipant(username: string): void {
    this.userService.getUser(username)
      .then(user => {
        this.participant = user;
        this.currentPageService.emitChange(CurrentPageService.prepareConversationAppPageInfo(this.participant));
        this.getConversationMostRecentPage()
          .then(conversation => {
            this.conversation = conversation;
            this.markMessagesAsReceived(this.conversation
              .filter(msg => (!msg.isReceived && msg.sentBy !== this.currentUser.username))
              .map(msg => msg.id));
            this.sortConversation();
            this.scrollToBottomBool = true;
          })
          .catch(err => console.log(err));
      })
      .catch(err => console.log(err));
  }

  private getConversationMostRecentPage(): Promise<Message[]> {
    return this.getConversationPage(0);
  }

  private getConversationPage(page: number): Promise<Message[]> {
    return this.conversationService.getConversation(this.participant.username, page, AppSettings.CONVERSATION_MESSAGES_PER_PAGE);
  }

  private startRefreshObservable() {
    this.refreshObservableSubscription = Observable.interval(AppSettings.REFRESH_INTERVAL / 2).takeWhile(() => true)
      .subscribe(() => {
        const updates: Updates = JSON.parse(localStorage.getItem('updates'));
        if (!updates) {
          return;
        }

        if (updates.participantUsernames.findIndex(u => u === this.participant.username) !== -1) {
          this.getConversationMostRecentPage()
            .then(conversation => {
              // this.conversation = conversation;
              conversation.forEach(msg => {
                if (this.conversation.findIndex(tmsg => tmsg.id === msg.id) === -1) {
                  this.conversation.push(msg);
                }
              });
              this.markMessagesAsReceived(this.conversation
                .filter(msg => (!msg.isReceived && msg.sentBy !== this.currentUser.username))
                .map(msg => msg.id));
              this.sortConversation();
              this.scrollToBottomBool = true;
            })
            .catch(err => console.log(err));
        }
      });
  }

  private markMessagesAsReceived(messageIds: number[]): void {
    if (messageIds.length < 1) {
      return;
    }
    this.conversationService.markMessagesAsReceived(messageIds)
      .then()
      .catch(err => console.log(err));
  }

}

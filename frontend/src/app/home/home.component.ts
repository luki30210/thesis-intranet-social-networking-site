import { MediaMatcher } from '@angular/cdk/layout';
import { Component, ChangeDetectorRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MatSidenav } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { AppSettings } from '../app.settings';
import { AppPage, Channel } from '../_model/app-page';
import { AuthService } from '../_service/auth.service';
import { ChannelComponent } from '../channel/channel.component';
import { ConversationComponent } from '../conversation/conversation.component';
import { CurrentPageService } from '../_shared/service/current-page.service';
import { ManageUsersComponent } from '../manage-users/manage-users.component';
import { NotificationService } from '../_service/notification.service';
import { SidebarChannelsComponent } from '../sidebar-channels/sidebar-channels.component';
import { SidebarContactsComponent } from '../sidebar-contacts/sidebar-contacts.component';
import { Updates, UpdatesService } from '../_service/updates.service';
import { User } from '../_service/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnDestroy, OnInit {

  public currentUser: User;
  isAdmin: boolean;
  currentPage: AppPage = { name: 'Strona główna', description: '', channelReference: undefined };

  @ViewChild(SidebarChannelsComponent) channelsViewChild: SidebarChannelsComponent;
  @ViewChild(SidebarContactsComponent) contactsViewChild: SidebarContactsComponent;

  conversationFlag = false;
  channelFlag = false;

  refreshObservableSubscription: Subscription;
  updatesPrevious: Updates = { channelIds: [], participantUsernames: [] };
  updates: Updates = {};

  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  constructor(
    private auth: AuthService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private currentPageService: CurrentPageService,
    private notificationService: NotificationService,
    private updatesService: UpdatesService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.isAdmin = this.isAdminTest(this.currentUser);
    this.currentPageService.changeEmitted$.subscribe(change => this.currentPage = change);
    if (this.refreshObservableSubscription === undefined || this.refreshObservableSubscription.closed) {
      this.startRefreshObservable();
    }
  }

  ngOnDestroy(): void {
    // this.mobileQuery.removeListener(this._mobileQueryListener);
    this.refreshObservableSubscription.unsubscribe();
  }

  onRouterOutletActivate(currentComponent: any) { }

  sidebarOnClick(nav: MatSidenav) {
    if (this.mobileQuery.matches) {
      nav.toggle();
    }
  }

  isAdminTest(user: User): boolean {
    return user.roles.find(r => r.name === AppSettings.ROLE_ADMIN) !== undefined;
  }

  onChannelNameEdit() {
    this.channelsViewChild.channelDesignerModal(this.currentPage.channelReference);
  }

  onChannelParticipantsEdit() {
    this.channelsViewChild.channelEditParticipantsModal(this.currentPage.channelReference, this.contactsViewChild.users);
  }

  logOut(): void {
    this.auth.logout();
    window.open('/', '_self');
  }

  private startRefreshObservable() {
    this.refreshObservableSubscription = Observable.interval(AppSettings.REFRESH_INTERVAL).takeWhile(() => true)
      .subscribe(() => {
        this.updatesService.getUpdates()
          .then(updates => {
            this.updates = updates;
            localStorage.setItem('updates', JSON.stringify(updates));
            this.showNotification();
            this.updatesPrevious = updates;
          })
          .catch(err => console.log(err));
      });
  }

  private showNotification() {
    if (this.updates.lastChannelEventTimestamp > this.updatesPrevious.lastChannelEventTimestamp) {
      this.notificationService.sendNotification('Nowe wydarzenie w kanale');
    }

    if (this.updates.lastMessageTimestamp > this.updatesPrevious.lastMessageTimestamp) {
      this.notificationService.sendNotification('Nowe wiadomość prywatna');
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { JwtHelper } from 'angular2-jwt';

import { AppSettings } from '../app.settings';
import { AuthService } from '../_service/auth.service';
import { User } from '../_service/user.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  statusCode: number;
  passwordMinLength: number = AppSettings.PASSWORD_MIN_LENGTH;

  constructor(private auth: AuthService, private router: Router, private formBuilder: FormBuilder, private snackBar: MatSnackBar) {
    this.createForm();
  }

  ngOnInit(): void {
    if (localStorage.getItem('justRegistered') != null) {
      this.snackBar.open('Nowoutworzone konto oczekuje na zatwierdzenie przez administratora', 'OK', {
        duration: 10000,
      });
      localStorage.removeItem('justRegistered');
    }
  }

  createForm(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(this.passwordMinLength)]]
    });
  }

  onSubmit(): void {
    const formData = this.loginForm.value;
    const user: User = new User();
    user.username = formData.username;
    user.password = formData.password;
    // let user: User = this.loginForm.value;

    this.auth.login(user)
      .then(res => {
        this.statusCode = res.status;
        const token = res.headers.get('Authorization');
        localStorage.setItem('token', token);

        const jwtHelper: JwtHelper = new JwtHelper();
        const appliactionUserFromToken = jwtHelper.decodeToken(token).appliactionUser;
        // console.log(JSON.stringify(jwtHelper.decodeToken(token)));
        // console.log(jwtHelper.getTokenExpirationDate(token));
        // console.log(jwtHelper.isTokenExpired(token));
        // console.log(appliactionUserFromToken)

        user.password = null;
        user.roles = appliactionUserFromToken.roles;

        if (user.roles.find(r => r.name === AppSettings.ROLE_USER) === undefined) {
          this.onLoginError('Konto nadal oczekuje na zatwierdzenie przez administratora');
          return;
        }

        user.email = appliactionUserFromToken.email;
        user.firstName = appliactionUserFromToken.firstName;
        user.lastName = appliactionUserFromToken.lastName;

        localStorage.setItem('currentUser', JSON.stringify(user));
        // console.log(user);

        this.router.navigate(['/']);
      })
      .catch((err) => {
        this.onLoginError('Błędne dane logowania');
        this.loginForm.get('password').setValue('');
      });

  }

  toRegister(): void {
    this.router.navigate(['/register']);
  }

  onLoginError(errorMsg: string): void {
    this.snackBar.open(errorMsg, 'OK', {
      duration: 3000,
    });
  }

  loginClicked() {
    if (!this.loginForm.valid) {
      console.log('x');
      Object.keys(this.loginForm.controls).map(x => this.loginForm.controls[x])
        .forEach(control => control.markAsTouched());
    }
  }

}

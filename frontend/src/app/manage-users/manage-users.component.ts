import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';

import { AppSettings } from '../app.settings';
import { ConfirmDialogComponent } from '../_shared/dialog/confirm-dialog/confirm-dialog.component';
import { CurrentPageService, AppPage } from '../_shared/service/current-page.service';
import { ModalType } from './modal-type.enum';
import { UserService, User, Role } from '../_service/user.service';
import { UsersTrashDialogComponent } from '../_shared/dialog/users-trash-dialog/users-trash-dialog.component';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ManageUsersComponent implements OnInit {

  private users: User[];
  currentUser: User;

  displayedColumns = ['username', 'firstName', 'lastName', 'email', 'actions'];
  dataSource: MatTableDataSource<User>;

  modalLabel: string;
  modalBodyText: string;
  private modalType: ModalType;

  constructor(
    private userService: UserService,
    private dialog: MatDialog,
    private currentPageService: CurrentPageService
  ) { }

  ngOnInit() {
    this.currentUser = <User>JSON.parse(localStorage.getItem('currentUser'));
    this.refreshUsersFromServer();
    setTimeout(() => this.currentPageService.emitChange(this.prepareAppPageInfo()), 0);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  onShowUserSelectionChange(selected: string) {
    const filterValue = this.dataSource.filter;
    if (selected === 'users') {
      const usersOnly = this.users.filter(u => u.roles.find(r => r.name === AppSettings.ROLE_USER) !== undefined);
      this.dataSource = new MatTableDataSource(usersOnly);
    } else if (selected === 'non-users') {
      const nonUsersOnly = this.users.filter(u => u.roles.find(r => r.name === AppSettings.ROLE_USER) === undefined);
      this.dataSource = new MatTableDataSource(nonUsersOnly);
    } else {
      this.dataSource = new MatTableDataSource(this.users);
    }
    this.dataSource.filter = filterValue;
  }

  private prepareAppPageInfo(): AppPage {
    const currentPage: AppPage = {
      name: 'Zarządzenie użytkownikami',
      description: 'Możesz nadać/odebrać prawa do korzystania z aplikacji oraz usunąć wybranych użytkowników'
    };
    return currentPage;
  }

  private refreshUsersFromServer() {
    this.userService.getAllUsers()
      .then(users => {
        this.users = users;
        this.dataSource = new MatTableDataSource(users);
      })
      .catch(err => {
        console.log(err);
      });
  }

  haveRoleUser(user: User): boolean {
    return UserService.haveRoleUser(user);
  }

  private setRoleUserToUser(user: User): void {
    this.userService.giveUserRole(user.username, AppSettings.ROLE_USER)
      .then(resp => {
        if (!this.haveRoleUser(user)) {
          const userRole = new Role();
          userRole.name = AppSettings.ROLE_USER;
          user.roles.push(userRole);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  private deleteRoleUserFromUser(user: User): void {
    this.userService.depriveUserRole(user.username, AppSettings.ROLE_USER).then(
      (resp) => {
        if (this.haveRoleUser(user)) {
          user.roles = user.roles.filter(r => r.name !== AppSettings.ROLE_USER);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  private discardUser(user: User): void {
    const username = user.username;
    this.userService.discardUser(username)
      .then(resp => {
        const deletedUserIndex = this.users.findIndex(u => u.username === username);
        this.users.splice(deletedUserIndex, 1);
        const currentFilter = this.dataSource.filter;
        this.applyFilter(currentFilter);
      })
      .catch(err => {
        console.log(err);
      });
  }

  confirmDelete(user: User): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '300px',
      data: {
        title: 'Usuwanie',
        question: 'Czy na pewno chcesz usunąć konto użytkownika <b>' + user.username + '</b>?'
      }
    });

    dialogRef.afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this.discardUser(user);
      }
    });
  }

  confirmAddUserRole(user: User): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '300px',
      data: {
        title: 'Nadawanie praw',
        question: 'Czy na pewno chcesz nadać użytkownikowi <b>' + user.username + '</b> prawa do korzystania z serwisu?'
      }
    });

    dialogRef.afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this.setRoleUserToUser(user);
      }
    });
  }

  confirmDepriveUserRole(user: User): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '300px',
      data: {
        title: 'Odbieranie praw',
        question: 'Czy na pewno chcesz odebrać użytkownikowi <b>' + user.username + '</b> prawa do korzystania z serwisu?'
      }
    });

    dialogRef.afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this.deleteRoleUserFromUser(user);
      }
    });
  }

  openUsersTrash(): void {
    const dialogRef = this.dialog.open(UsersTrashDialogComponent, {
      width: '500px',
      position: { top: '60px' }
    });
  }

}

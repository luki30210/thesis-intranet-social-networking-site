import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, EmailValidator } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router/*, ActivatedRoute*/ } from '@angular/router';

import { AppSettings } from '../app.settings';
import { AuthService } from '../_service/auth.service';
import { User } from '../_service/user.service';
import { UsernameValidator } from '../register/validator/username.validator';

@Component({
  // selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./../login/login.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  passwordMinLength = AppSettings.PASSWORD_MIN_LENGTH;

  constructor(
    private auth: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private usernameValidator: UsernameValidator
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm(): void {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required, this.usernameValidator.checkUsername.bind(this.usernameValidator)],
      password: ['', [Validators.required, Validators.minLength(this.passwordMinLength)]],
      passwordConfirm: ['', [Validators.required, Validators.minLength(this.passwordMinLength)]],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  onSubmit(): void {
    const formValue = this.registerForm.value;
    const user: User = new User();
    user.username = formValue.username;
    user.password = formValue.password;
    user.firstName = formValue.firstName;
    user.lastName = formValue.lastName;
    user.email = formValue.email;

    this.auth.register(user)
      .then(res => {
        localStorage.setItem('justRegistered', 'yes');
        this.router.navigate(['/log-in']);
      })
      .catch(err => {
        if (err.status === 409) {
          this.snackBar.open('Użytkownik już istnieje', 'OK', {
            duration: 10000,
          });
        }
      });
  }

  toLogin(): void {
    this.router.navigate(['/log-in']/*, {relativeTo: this.aRoute}*/);
  }

  registerClicked() {
    if (!this.registerForm.valid) {
      Object.keys(this.registerForm.controls).map(x => this.registerForm.controls[x])
        .forEach(control => control.markAsTouched());
    }
  }

}

import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AuthService } from '../../_service/auth.service';

@Injectable()
export class UsernameValidator {

  debouncer: any;

  constructor(public authService: AuthService) { }

  checkUsername(control: FormControl): any {
    clearTimeout(this.debouncer);
    return new Promise(resolve => {
      this.debouncer = setTimeout(() => {
        this.authService.isUsernameFree(control.value)
          .then(resp => resolve(null))
          .catch((err: Response) => err.status === 409 ? resolve({ 'usernameInUse': true }) : null);
      }, 1000);
    });
  }

}

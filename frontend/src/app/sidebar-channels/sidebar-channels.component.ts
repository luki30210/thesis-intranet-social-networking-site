import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Channel, ChannelService } from '../_service/channel.service';
import {
  ChannelEditParticipantsDialogComponent
} from '../_shared/dialog/channel-edit-participants-dialog/channel-edit-participants-dialog.component';
import { ChannelDesignerDialogComponent } from '../_shared/dialog/channel-designer-dialog/channel-designer-dialog.component';
import { CurrentPageService } from '../_shared/service/current-page.service';
import { Updates } from '../_service/updates.service';
import { User } from '../_service/user.service';

@Component({
  selector: 'app-sidebar-channels',
  templateUrl: './sidebar-channels.component.html',
  styleUrls: ['./sidebar-channels.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SidebarChannelsComponent implements OnChanges, OnInit {

  channels: Channel[];

  @Input() updates: Updates;

  constructor(
    private channelService: ChannelService,
    private dialog: MatDialog,
    private currentPageService: CurrentPageService
  ) { }

  ngOnInit() {
    this.getChannels();
  }

  ngOnChanges() {
    this.checkNewEvents();
  }

  createChannelModal(): void {
    const dialogRef = this.dialog.open(ChannelDesignerDialogComponent, {
      width: '500px',
      position: { top: '60px' },
      data: {}
    });

    dialogRef.afterClosed().subscribe((newChannel: Channel) => {
      if (newChannel) {
        this.createChannel(newChannel);
      }
    });
  }

  channelDesignerModal(channel: Channel): void {
    const dialogRef = this.dialog.open(ChannelDesignerDialogComponent, {
      width: '500px',
      position: { top: '60px' },
      data: { channel: channel }
    });

    dialogRef.afterClosed().subscribe((editedChannel: Channel) => {
      if (editedChannel) {
        this.updateChannel(editedChannel);
      }
    });
  }

  channelEditParticipantsModal(channel: Channel, allUsers: User[]): void {
    const dialogRef = this.dialog.open(ChannelEditParticipantsDialogComponent, {
      width: '500px',
      position: { top: '60px' },
      data: { channel: channel, allUsers: allUsers }
    });

    dialogRef.afterClosed().subscribe(newParticipantsList => {
      if (newParticipantsList) {
        const updatedChannel: Channel = {
          id: channel.id,
          name: channel.name,
          leader: channel.leader,
          channelParticipants: newParticipantsList
        };
        this.updateChannel(updatedChannel);
      }
    });
  }

  private createChannel(channel: Channel) {
    this.channelService.postChannel(channel.name)
      .then(newChannel => this.channels.push(newChannel))
      .catch(err => console.log(err));
  }

  private updateChannel(channel: Channel) {
    this.channelService.patchChannel(channel)
      .then(newChannel => {
        const oldChannel = this.channels.find(ch => ch.id === newChannel.id);
        oldChannel.name = newChannel.name;
        oldChannel.leader = newChannel.leader;
        oldChannel.channelParticipants = newChannel.channelParticipants;
        this.currentPageService.emitChange(CurrentPageService.prepareChannelAppPageInfo(oldChannel));
      })
      .catch(err => console.log(err));
  }

  private getChannels() {
    this.channelService.getChannels()
      .then(channels => {
        this.channels = channels;
      })
      .catch(err => console.log(err));
  }

  private checkNewEvents() {
    if (this.channels === undefined) {
      return;
    }
    this.channels.forEach(channel => {
      channel.newEvents = this.updates.channelIds.findIndex(ci => ci === channel.id) !== -1;
    });
  }

}

import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AppPage } from '../_model/app-page';
import { ConversationService, Message } from '../_service/conversation.service';
import { Updates } from '../_service/updates.service';
import { UserService, User } from '../_service/user.service';

@Component({
  selector: 'app-sidebar-contacts',
  templateUrl: './sidebar-contacts.component.html',
  styleUrls: ['../sidebar-channels/sidebar-channels.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SidebarContactsComponent implements OnChanges, OnDestroy, OnInit {

  users: User[];
  currentUser: User;

  @Input() updates: Updates;

  constructor(
    private messageService: ConversationService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.refreshUsersFromServer();
  }

  ngOnChanges() {
    this.checkNewMessages();
  }

  ngOnDestroy() { }

  private refreshUsersFromServer() {
    this.userService.getAllUsersSortedByConversationTimestamp()
      .then(users => {
        const onlyUsersWithPermissions = users.filter(u => UserService.haveRoleUser(u));
        this.users = onlyUsersWithPermissions.filter(u => u.username !== this.currentUser.username);
      })
      .catch(err => console.log(err));
  }

  private checkNewMessages() {
    if (this.users === undefined) {
      return;
    }
    this.users.forEach(user => {
      user.newMessages = this.updates.participantUsernames.findIndex(u => u === user.username) !== -1;
    });
  }

}
